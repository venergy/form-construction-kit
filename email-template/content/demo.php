<?php

	/**
	 * Email variables
	 */

	//accept email variables set in the form config

	$emailTo = $successAction['email-template']['email-to'];
	$emailToCC = (isset($successAction['email-template']['email-to-cc'])) ? $successAction['email-template']['email-to-cc'] : [];
	$emailToBCC = (isset($successAction['email-template']['email-to-bcc'])) ? $successAction['email-template']['email-to-bcc'] : [];
	$emailSubject = $successAction['email-template']['email-subject'];
	$emailFrom = $successAction['email-template']['email-from'];
	$emailFromName = $successAction['email-template']['email-from-name'];

	//or declare your variables here. eg.

	// $emailTo = ["recipient@email.com"];
	// $emailToCC = ["recipient@email.com"];
	// $emailToBCC = ["recipient@email.com"];
	// $emailSubject = "Subject";
	// $emailFrom = "sender@email.com";
	// $emailFromName = "Mr Email Sender";

	//dkim
	// $dkimOn = true;
	// $dkimDomain = 'example.com';
	// $dkimPrivateKey = "<private key location>";
	// $dkimSelector = '<selector name>';

	$emailContentHTML = "";

	/**
	 * Content html
	 */

	//personalised introduction example
	$emailContentHTML .= "<b>Hello ".$this->getElementResponse('Field (with custom css)')."</b>";
	$emailContentHTML .= "<br/><br/>";
	$emailContentHTML .= "Velit tempor sit aliquip velit ipsum aute velit labore Lorem eu exercitation ullamco fugiat dolore. Enim do consequat irure quis duis aute labore id mollit qui deserunt veniam. Ad ad enim proident excepteur culpa minim ipsum incididunt irure aute. Velit tempor consectetur est cillum culpa do excepteur occaecat est ea culpa sint excepteur. Pariatur reprehenderit pariatur ipsum cillum cillum exercitation reprehenderit exercitation laborum fugiat elit officia quis ut.";
	$emailContentHTML .= "<br/><br/>";

	//retrieve user details (recipient) example
	$emailContentHTML .= "Applicant's email address (for auto reply): ".$this->getElementResponse('email-address');
	$emailContentHTML .= "<br/><br/>";

	$emailContentHTML .= "Confirm your email address - <a href='".$this->getEmailVerificationUrl()."'>Click Here</a>.";

	$emailContentHTML .= "<h3>Applicant's responses:</h3>";
	
	//get form structured form content example
	$emailContentHTML .= $this->getStructuredFormContent(false);