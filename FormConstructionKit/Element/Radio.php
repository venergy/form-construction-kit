<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Radio
	 */

	class Radio extends FormElement {

		public function getHTML() : string {

			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-radio ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$elementLabel = "<span class='form-element-label-title'>".$this->label."</span>";
			$validationIndicator = (isset($this->validation) && count($this->validation) > 0) ? "<span class='mandatory-element-identifier'>*</span>" : '';
			$validationAlert = (!$this->isValid) ? "<span class='validation-error'>".$this->validationMessage."</span>" : '';

			$html .= "<label class='form-element-label form-element-label-radio'>".$elementLabel.$validationIndicator.$validationAlert."</label>";

			if(!empty($this->description)) {
				$html .= "<div class='form-element-description'>".$this->description."</div>";
			}

			$radioIndex = 0;

			$elementClass = (isset($this->elementClass)) ? $this->elementClass : '';

			$html .= "<div class='form-element-group-component-radio'>";

			foreach($this->elementContent AS $radioText => $radioValue) {

				//sub element variables
				$radioId = $this->id.'-'.$radioIndex;

				$checked = ($this->response == $radioValue) ? 'checked="checked"' : '';

				$parentId = "parent-id='".$this->id."'";
				$conditionalChildren = '';

				if(!empty($this->responseDisplays)){
					$conditionalChildren .= "conditional-children='";
					if(isset($this->responseDisplays[$radioValue])) {
						for($i=0;$i<count($this->responseDisplays[$radioValue]);$i++) {
							if($i>0){$conditionalChildren .= " ";}
							$conditionalChildren .= $this->responseDisplays[$radioValue][$i];
						}
					}
					$conditionalChildren .= "'";
				}

				//html
				$html .= "<span class='form-element-component-radio'>";
				$html .= "<input type='radio' id='".$radioId."' name='".$this->id."' value=\"".$radioValue."\" class='".$elementClass."' ".$checked." ".$parentId." ".$conditionalChildren.">";
				$html .= "<label for='".$radioId."'> ".$radioText."</label>";
				$html .= "</span>";

				$radioIndex++;

			}

			$html .= "</div>";
			
			$html .= "</div>";

			return $html;
		}

	}