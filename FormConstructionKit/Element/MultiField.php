<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Multi Field
	 */

	class Multifield extends FormElement {

		public function getHTML() : string {
			
			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-multi-field ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$elementLabel = "<span class='form-element-label-title'>".$this->label."</span>";
			$validationIndicator = (isset($this->validation) && count($this->validation) > 0) ? "<span class='mandatory-element-identifier'>*</span>" : '';
			$validationAlert = (!$this->isValid) ? "<span class='validation-error'>".$this->validationMessage."</span>" : '';

			$html .= "<label class='form-element-label form-element-label-multi-field' for='".$this->id."[]'>".$elementLabel.$validationIndicator.$validationAlert."</label>";

			if(!empty($this->description)) {
				$html .= "<div class='form-element-description'>".$this->description."</div>";
			}

			$elementClass = (isset($this->elementClass)) ? $this->elementClass : '';

			$buttonValue = (isset($this->buttonValue) && !empty($this->buttonValue)) ? $this->buttonValue : 'Add field';

			$inputCount = 0;

			//Note: Initial MultiForm load response will always return a blank string. Submission will always return an array.

			if(is_array($this->response)) {

				for ($i = 0; $i < count($this->response); $i++) {

					if(!empty($this->response[$i]) || (empty($this->response[$i]) && count($this->response) == 1)) {

						if($inputCount>0) {$html .= "<br/>";}

						$html .= "<input type='text' id ='".$this->id."-".$i."' name='".$this->id."[]' class='".$elementClass."' value=\"".$this->response[$i]."\" placeholder='".$this->placeholder."'/>";

						$inputCount++;

					}
                }

			}
			
			if($inputCount == 0){
				$html .= "<input type='text' id='".$this->id."' name='".$this->id."[]' value='' class='".$elementClass."' placeholder='".$this->placeholder."'/>";
			}

			$html .= "<div id='".$this->id."-additional-fields'></div>";

			$html .= "<input type='button' id='".$this->id."-button' value='$buttonValue' onclick='addMultifield".$this->getCamelCaseFromID($this->id)."()'/>";

			//javascript to add additional fields
			$html .= "<script type='text/javascript'>";
			$html .= "function addMultifield".$this->getCamelCaseFromID($this->id)."(){";
			$html .= "var input = document.createElement(\"\input\");";
			$html .= "input.setAttribute(\"type\",\"text\");";
			$html .= "input.setAttribute(\"name\",\"".$this->id."[]\");";
			$html .= "input.setAttribute(\"placeholder\",\"".$this->placeholder."\");";
			$html .= "document.getElementById(\"".$this->id."-additional-fields\").appendChild(input);";
			$html .= "var br = document.createElement(\"br\");";
			$html .= "document.getElementById(\"".$this->id."-additional-fields\").appendChild(br);";
			$html .= "}";
			$html .= "</script>";
			
			$html .= "</div>";

			return $html;
		}

		private function getCamelCaseFromID(String $id) {
			return str_replace("-","",ucwords($id,"-"));
		}
	}