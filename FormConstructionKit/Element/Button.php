<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Button
	 */

	class Button extends FormElement {

		public function getHTML() : string {

			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$buttonValue = (isset($this->buttonValue) && !empty($this->buttonValue)) ? $this->buttonValue : 'Button';

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-button ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$html .= "<input type='button' id='".$this->id."' name='".$this->id."' value='$buttonValue' />";

			$html .= "</div>";

			return $html;
		}

	}