<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Checkbox
	 */

	class Checkbox extends FormElement {

		public function getHTML() : string {

			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-checkbox ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$elementLabel = "<span class='form-element-label-title'>".$this->label."</span>";
			$validationIndicator = (isset($this->validation) && count($this->validation) > 0) ? "<span class='mandatory-element-identifier'>*</span>" : '';
			$validationAlert = (!$this->isValid) ? "<span class='validation-error'>".$this->validationMessage."</span>" : '';

			$html .= "<label class='form-element-label form-element-label-checkbox'>".$elementLabel.$validationIndicator.$validationAlert."</label>";

			if(!empty($this->description)) {
				$html .= "<div class='form-element-description'>".$this->description."</div>";
			}

			if(!empty($this->elementContent)) {

				$checkboxIndex = 0;

				$elementClass = (isset($this->elementClass)) ? $this->elementClass : '';

				$html .= "<div class='form-element-group-component-checkbox'>";

				foreach($this->elementContent AS $checkboxText => $checkboxValue) {

					//sub element variables
					$checkboxId = $this->id.'-'.$checkboxIndex;

					$checked = (gettype($this->response) == 'array' && in_array($checkboxValue,$this->response)) ? 'checked="checked"' : '';

					$parentId = "parent-id='".$this->id."'";
					$conditionalChildren = '';

					if(!empty($this->responseDisplays)){
						$conditionalChildren .= "conditional-children='";
						if(isset($this->responseDisplays[$checkboxValue])) {
							for($i=0;$i<count($this->responseDisplays[$checkboxValue]);$i++) {
								if($i>0){$conditionalChildren .= " ";}
								$conditionalChildren .= $this->responseDisplays[$checkboxValue][$i];
							}
						}
						$conditionalChildren .= "'";
					}

					//html
					$html .= "<span class='form-element-component-checkbox'>";
					$html .= "<input type='checkbox' id='".$checkboxId."' name='".$this->id."[]' value=\"".$checkboxValue."\" class='".$elementClass."' ".$checked." ".$parentId." ".$conditionalChildren.">";
					$html .= "<label for='".$checkboxId."'> ".$checkboxText."</label>";
					$html .= "</span>";

					$checkboxIndex++;

				}

				$html .= "</div>";

				$html .= "</div>";

			}

			return $html;
		}

	}