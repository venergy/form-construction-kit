<?php

	namespace Form\Element;

	use Form\FormElement;
	use Form\Helpers\FormHelpers;

	require_once dirname(__FILE__)."/FormElement.php";
	require_once dirname(__FILE__)."/../FormHelpers.php";

	/**
	 * Form Element Content
	 */

	class Content extends FormElement {

		public function getHtml() : string {

			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-content ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$html .= FormHelpers::getHtml($this->htmlContent);

			$html .= "</div>";

			return $html;
		}

	}