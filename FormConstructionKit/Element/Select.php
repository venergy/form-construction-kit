<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Select
	 */

	class Select extends FormElement {

		public function getHTML() : string {

			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-select ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$elementLabel = "<span class='form-element-label-title'>".$this->label."</span>";
			$validationIndicator = (isset($this->validation) && count($this->validation) > 0) ? "<span class='mandatory-element-identifier'>*</span>" : '';
			$validationAlert = (!$this->isValid) ? "<span class='validation-error'>".$this->validationMessage."</span>" : '';

			$html .= "<label class='form-element-label form-element-label-select' for='".$this->id."'>".$elementLabel.$validationIndicator.$validationAlert."</label>";

			if(!empty($this->description)) {
				$html .= "<div class='form-element-description'>".$this->description."</div>";
			}

			$elementClass = (isset($this->elementClass)) ? $this->elementClass : '';

			$hasConditionalChildren = "has-conditional-children='false'";
			if(!empty($this->responseDisplays)) {
				$hasConditionalChildren = "has-conditional-children='true'";
			}

			$html .= "<select id='".$this->id."' name='".$this->id."' class='".$elementClass."' ".$hasConditionalChildren.">";

			if(!empty($this->elementContent)) {
				foreach($this->elementContent AS $optionText => $optionValue) {
					$selected = ($this->response == $optionValue) ? 'selected="selected"' : '';

					$conditionalChildren = '';
					if(!empty($this->responseDisplays)){
						$conditionalChildren .= "conditional-children='";
						if(isset($this->responseDisplays[$optionValue])) {
							for($i=0;$i<count($this->responseDisplays[$optionValue]);$i++) {
								if($i>0){$conditionalChildren .= " ";}
								$conditionalChildren .= $this->responseDisplays[$optionValue][$i];
							}
						}
						$conditionalChildren .= "'";
					}
					
					$html .= "<option value=\"".$optionValue."\" ".$selected." ".$conditionalChildren.">".$optionText."</option>";
				}
			}

			$html .= "</select>";

			$html .= "</div>";

			return $html;
		}

	}