<?php

	namespace Form;

	/**
	 * Abstract Form Element class
	 */

	abstract class FormElement {

		protected $label;
		protected $hideLabel;
		protected $id;
		protected $description;
		protected $elementContent;
		protected $placeholder;
		protected $elementClass;
		protected $containerClass;
		protected $response;
		protected $validation;
		protected $isValid;
		protected $validationMessage;
		protected $gRecaptchaDataSiteKey;
		protected $gRecaptchaSecret;
		protected $htmlContent;
		protected $isVisible;
		protected $responseDisplays;
		protected $variableElements;
		protected $elementParents;
		protected $fieldType;
		protected $buttonValue;
		protected $code;

		public function __construct(Array &$elementData, Array &$postData, bool $formSubmitted) {

			//element label
			$this->label = (isset($elementData['label'])) ? $elementData['label'] : '';

			//element id
			$this->id = (isset($elementData['id'])) ? $elementData['id'] : '';

			//element description
			$this->description = (isset($elementData['description-text'])) ? $elementData['description-text'] : '';

			//element content
			$this->elementContent = (isset($elementData['element-content'])) ? $elementData['element-content'] : '';

			//element placeholder
			$this->placeholder = (isset($elementData['placeholder'])) ? $elementData['placeholder'] : '';

			//element class
			$this->elementClass = (isset($elementData['element-class'])) ? $elementData['element-class'] : '';

			//container class
			$this->containerClass = (isset($elementData['container-class'])) ? $elementData['container-class'] : '';

			//element response
			$this->response = (isset($elementData['response'])) ? $elementData['response'] : '';

			//element validators
			$this->validation = (isset($elementData['validation'])) ? $elementData['validation'] : [];

			//element validity
			$this->isValid = (isset($elementData['is-valid']) && $formSubmitted) ? $elementData['is-valid'] : true;

			//validation message
			$this->validationMessage = (isset($elementData['validation-message'])) ? $elementData['validation-message'] : '';

			//recaptcha site key
			$this->gRecaptchaDataSiteKey = (isset($elementData['g-recaptcha-data-site-key'])) ? $elementData['g-recaptcha-data-site-key'] : '';

			//recaptcha secret
			$this->gRecaptchaSecret = (isset($elementData['g-recaptcha-secret'])) ? $elementData['g-recaptcha-secret'] : '';

			//html content
			$this->htmlContent = (isset($elementData['html-content'])) ? $elementData['html-content'] : [];

			//is element visible (variable element)
			$this->isVisible = (isset($elementData['is-visible'])) ? $elementData['is-visible'] : false;

			//response displays
			$this->responseDisplays = (isset($elementData['response-displays'])) ? $elementData['response-displays'] : [];

			//element parents
			$this->elementParents = (isset($elementData['element-parents'])) ? $elementData['element-parents'] : [];

			//field type
			$this->fieldType = (isset($elementData['field-type'])) ? $elementData['field-type'] : '';

			//button-value
			$this->buttonValue = (isset($elementData['button-value'])) ? $elementData['button-value'] : '';

			//code
			$this->code = (isset($elementData['code'])) ? $elementData['code'] : '';
		}

		abstract public function getHTML() : string;
	}