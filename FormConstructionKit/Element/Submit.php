<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Submit
	 */

	class Submit extends FormElement {

		public function getHTML() : string {

			$html = "";

			$buttonValue = (isset($this->buttonValue) && !empty($this->buttonValue)) ? $this->buttonValue : 'Submit';

			$html .= "<div class='form-element-container form-element-container-submit ".$this->containerClass."'>";

			$html .= "<input type='submit' id='".$this->id."' name='".$this->id."' value='$buttonValue' />";

			$html .= "</div>";

			return $html;
		}

	}