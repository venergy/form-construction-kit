<?php

namespace Form\Element;

use Form\FormElement;

require_once dirname(__FILE__)."/FormElement.php";

/**
 * Form Element Hiddenfield
 */

class Hiddenfield extends FormElement {

	public function getHTML() : string {

		return "<input type='hidden' id='".$this->id."' name='".$this->id."' value=\"".$this->response."\"/>";
	}
}