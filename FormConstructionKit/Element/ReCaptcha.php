<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Recaptcha class
	 */

	class Recaptcha extends FormElement{

		public function getHTML() : string {
			$html = "";

			$html .= "<div class='form-element-container form-element-container-recaptcha ".$this->containerClass."'>";

			$elementLabel = "<span class='form-element-label-title'>".$this->label."</span>";
			$validationIndicator = (isset($this->validation) && count($this->validation) > 0) ? "<span class='mandatory-element-identifier'>*</span>" : '';
			$validationAlert = (!$this->isValid) ? "<span class='validation-error'>".$this->validationMessage."</span>" : '';
			$labelClass = ($this->hideLabel) ? 'hide' : '';

			$html .= "<label class='form-element-label form-element-label-recaptcha' for='".$this->id."'>".$validationAlert."</label>";

			if(!empty($this->description)) {
				$html .= "<div class='form-element-description'>".$this->description."</div>";
			}

			$html .= "<div class='g-recaptcha' data-sitekey='".$this->gRecaptchaDataSiteKey."'></div>";
			
			$html .= "</div>";

			return $html;
		}
	}