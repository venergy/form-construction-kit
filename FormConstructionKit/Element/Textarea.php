<?php

	namespace Form\Element;

	use Form\FormElement;

	require_once dirname(__FILE__)."/FormElement.php";

	/**
	 * Form Element Textarea
	 */

	class Textarea extends FormElement {

		public function getHTML() : string {

			$html = "";

			$elementVisibilityClass = ($this->isVisible) ? '' : 'hide';

			$elementParents = implode(" ",$this->elementParents);

			$html .= "<div id='element-container-".$this->id."' class='form-element-container form-element-container-textarea ".$this->containerClass." ".$elementVisibilityClass." ".$elementParents."'>";

			$elementLabel = "<span class='form-element-label-title'>".$this->label."</span>";
			$validationIndicator = (isset($this->validation) && count($this->validation) > 0) ? "<span class='mandatory-element-identifier'>*</span>" : '';
			$validationAlert = (!$this->isValid) ? "<span class='validation-error'>".$this->validationMessage."</span>" : '';

			$html .= "<label class='form-element-label form-element-label-textarea' for='".$this->id."'>".$elementLabel.$validationIndicator.$validationAlert."</label>";

			if(!empty($this->description)) {
				$html .= "<div class='form-element-description'>".$this->description."</div>";
			}

			$elementClass = (isset($this->elementClass)) ? $this->elementClass : '';

			$html .= "<textarea id='".$this->id."' name='".$this->id."' class='".$elementClass."' placeholder='".$this->placeholder."' rows='4'>".$this->response."</textarea>";
			
			$html .= "</div>";

			return $html;
		}

	}