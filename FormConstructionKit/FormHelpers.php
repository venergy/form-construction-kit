<?php

	namespace Form\Helpers;

	class FormHelpers {

		public function __construct()
		{
			
		}

		public static function getHtml(array $htmlContent) {

			$html = "";

			for($i=0;$i<count($htmlContent);$i++) {
				if(getType($htmlContent[$i]) == 'array') {
					$elementClass = (isset($htmlContent[$i][2])) ? $htmlContent[$i][2] : '';
					$html .= "<".$htmlContent[$i][0]." class='".$elementClass."'>".$htmlContent[$i][1]."</".$htmlContent[$i][0].">";
				}
			}

			return $html;

		}
	}