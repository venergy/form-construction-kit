<?php 

	namespace Form;

	use Form\Helpers\FormHelpers;

	//PDO namespaces
	use \PDO;
	use \PDOException;

	require dirname(__FILE__).'/FormConstructionKit.php';

	class EmailValidation extends FormConstructionKit{

		private $html;

		public function validate() {

			$host = $this->formOptions['database-credentials']['host'];
			$database = $this->formOptions['database-credentials']['database'];
			$table = $this->formOptions['database-credentials']['table'];
			$username = $this->formOptions['database-credentials']['username'];
			$password = $this->formOptions['database-credentials']['password'];
			$formTemplate = $this->formOptions['form-template'];

			//prepare PDO
			$dsn = 'mysql:dbname='.$database.';host='.$host;
			
			//new php database object (pdo)
			try {
				$pdo = new PDO($dsn,$username,$password);
			}
			catch(PDOException $e) {
				die('oops:'.$e);
			}

			//MySQL
			$pdo->exec('SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;');
			$pdo->exec('SET SESSION sql_mode = \'ANSI\';');

			//Development
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			//True Prepared Statements
			$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);

			//Extend PDOStatement
			$pdo->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('DBStatement',array($pdo)));

			//check $_GET['sdt'] (submission date time) && $_GET['suid'] (submission unique id)
			$sdt = (isset($_GET['sdt']) && !empty($_GET['sdt'])) ? $_GET['sdt'] : '';
			$suid = (isset($_GET['suid']) && !empty($_GET['suid'])) ? $_GET['suid'] : '';

			//search for row matching datetime AND unique submission id
			$query = "SELECT * FROM ".$table." WHERE date = ? AND uid = ?";
			$prepared = $pdo->prepare($query);
			$prepared->execute([$sdt,$suid]);
			$results = $prepared->fetch(); //fetch one row only

			if ($prepared->rowCount() > 0) {
				//if row is exists update row validated = 1
				$queryUpdate = "UPDATE ".$table." SET valid = 1 WHERE date = ? AND uid = ?";
				$preparedUpdate = $pdo->prepare($queryUpdate);
				$preparedUpdate->execute([$sdt,$suid]);

				$confirmationMessage = $this->getTemplateVariable('validation-success-html');
			} else {
				$confirmationMessage = $this->getTemplateVariable('validation-fail-html');
			}

			$this->html = FormHelpers::getHtml($confirmationMessage);

			require_once "form-template/".$formTemplate.".php";

		}
	}
