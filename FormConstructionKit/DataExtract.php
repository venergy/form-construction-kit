<?php

	require dirname(__FILE__).'/DbStatement.php';

	class DataExtract {

		public function __construct()
		{
			
		}

		public static function extractToCsv(String $host, String $database, String $table, String $username, String $password, String $priKey, String $fileSaveName) {

			//prepare PDO
			$dsn = 'mysql:dbname='.$database.';host='.$host;
			
			//new php database object (pdo)
			try {
				$pdo = new PDO($dsn,$username,$password);
			}
			catch(PDOException $e) {
				die('oops:'.$e);
			}

			//MySQL
			$pdo->exec('SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;');
			$pdo->exec('SET SESSION sql_mode = \'ANSI\';');

			//Development
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			//True Prepared Statements
			$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);

			//Extend PDOStatement
			$pdo->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('DBStatement',array($pdo)));

			//extract data
			$query = "SELECT * FROM ".$table;
			$prepared = $pdo->prepare($query);
			$prepared->execute();
			$results = $prepared->fetchAll();

			//set server timezone
			date_default_timezone_set("Australia/Melbourne");

			//get current time for file name
			$currentDateTime = date('d-m-Y-g-ia',time());

			//set headers
			header("Content-type: text/csv");
			header("Content-Disposition: attachment; filename=".$fileSaveName."-extract-".$currentDateTime.".csv");
			header("Pragma: no-cache");
			header("Expires: 0");

			//extract data row by row
			$rowCount = 0;

			foreach ($results as $row) {

				$rowId = $row['id'];
				$rowDate = $row['date'];

				$rowData = $row['data'];

				if(file_exists('keys/private/'.$priKey.'.key')) {
					$encryptedData = base64_decode($rowData);
					$privateKey = file_get_contents('keys/private/'.$priKey.'.key');
					openssl_private_decrypt($encryptedData, $decryptedSerializedData, $privateKey);
					$unserializedDataArray = unserialize($decryptedSerializedData);
				} else {
					$unserializedDataArray = unserialize($rowData);
				}

				//test output
				// echo "<pre>";
				// print_r($unserializedDataArray);
				// echo "</pre>";

				//echo out header row if first loop
				if($rowCount == 0) {
					echo "id,date";
					foreach($unserializedDataArray AS $key => $value) {
						echo ",".$key;
					}

					//line end
					echo "\n";
				}

				//echo out data
				echo $rowId;
				echo ',';
				echo date('d/m/Y g:ia',$rowDate);

				foreach($unserializedDataArray AS $key => $value) {
					echo ',"'.DataExtract::cleanDataForCSV($value).'"';
				}

				//line end
				echo "\n";

				$rowCount++;
			}

		}

		private static function cleanDataForCSV(string $data){

			$data = str_replace(['  '],' ',$data);
	
			return $data;
		}
	}