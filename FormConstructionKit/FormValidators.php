<?php

	namespace Form\Validators;

	class FormValidators {

		public function notEmpty($value,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldHasAValue = false;
				for($i=0;$i<count($value);$i++) {
					if(!empty($value[$i])){$multiFieldHasAValue = true;}
				}
				return $multiFieldHasAValue;
			} else {
				return (empty($value)) ? false : true;
			}
		}

		public function notFirstSelect($value,$firstValue) {
			//return bool isValid
			return ($value == $firstValue) ? false : true;
		}

		public function alphaOnly($value,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(!preg_match("/^[a-zA-Z-' ]*$/",$value[$i])){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				return (preg_match("/^[a-zA-Z-' ]*$/",$value)) ? true : false;
			}
		}

		public function numericOnly($value,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(!preg_match("/^[0-9-' ]*$/",$value[$i])){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				return (preg_match("/^[0-9-' ]*$/",$value)) ? true : false;
			}
		}

		public function alphaNumericOnly($value,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(!preg_match("/^[a-zA-Z0-9-' ]*$/",$value[$i])){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				return (preg_match("/^[a-zA-Z0-9-' ]*$/",$value)) ? true : false;
			}
		}

		public function minChars($value,$min,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(!empty($value[$i]) && strlen($value[$i]) < $min){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				return (strlen($value) >= $min) ? true : false;
			}
		}

		public function maxChars($value,$max,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(strlen($value[$i]) > $max){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				return (strlen($value) <= $max) ? true : false;
			}
		}

		public function validEmail($value,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(!empty($value[$i]) && !filter_var($value[$i], FILTER_VALIDATE_EMAIL)){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				if(!empty(trim($value))){
					return (filter_var($value, FILTER_VALIDATE_EMAIL)) ? true : false;
				} else {
					return true;
				}
			}
		}

		public function matchInput($value,$responseTarget) {
			//return bool isValid
			if($value == $responseTarget) {
				return true;
			} else {
				return false;
			}
		}

		public function validUrl($value,$isMultiField) {
			//return bool isValid
			if($isMultiField) {
				$multiFieldIsValid = true;
				for($i=0;$i<count($value);$i++) {
					if(!empty($value[$i]) && !preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$value[$i])){$multiFieldIsValid = false;}
				}
				return $multiFieldIsValid;
			} else {
				return (preg_match("/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",$value)) ? true : false;
			}
		}

		public function validRecaptcha($gRecaptchaResponse,$gRecaptchaSecret) {
			//return bool isValid

			$data = [
				'secret' => $gRecaptchaSecret,
				'response' => $gRecaptchaResponse
			];

			$curl = curl_init();

			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
			curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));

			$response = curl_exec($curl);
			$response = json_decode($response, true);

			return (isset($response['success']) && $response['success'] === true) ? true : false;

		}

		public function validFileType($value,$acceptedFileTypes) {
			//return bool isValid

			//get file extension
			$filenameExtension = $this->getFileExtension($value);

			//check if extension is allowed
			return (strpos($acceptedFileTypes, $filenameExtension) !== false) ? true : false;
		}

		public function validFileSize($value,$fileSize,$fileSizeLimit) {
			//return bool isValid
			$fileSizeLimit = $this->toBytes($fileSizeLimit);
			return ($fileSize <= $fileSizeLimit) ? true : false;
		}

		private function getFileExtension(String $filename) {
			$path_info = pathinfo($filename);
			return $path_info['extension'];
		}

		private function toBytes(String $size) {
			$size = trim(strtolower($size));
			$suffix = substr($size,-2);

			if($suffix == 'gb') {
				$gbSize = (int)str_replace('gb','',$size);
				return $gbSize * 1024 * 1024 * 1024;
			} else if ($suffix ==  'mb') {
				$mbSize = (int)str_replace('mb','',$size);
				return $mbSize * 1024 * 1024;
			} else if ($suffix == 'kb') {
				$kbSize = (int)str_replace('kb','',$size);
				return $kbSize * 1024;
			} else {
				//if $size hasn't been defined with the correct suffix return 0
				return 0;
			}
		}

	}