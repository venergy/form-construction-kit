<?php

	class GenerateKeys {

		public function __construct()
		{
			
		}

		public static function generate () {

			//GENERATE PUBLIC AND PRIVATE KEYS

			$currentDateTime = time();

			//check for directories and create if they don't exist
			if(!file_exists('../keys')){mkdir('../keys');}
			if(!file_exists('../keys/public')){mkdir('../keys/public');}
			if(!file_exists('../keys/private')){mkdir('../keys/private');}

			/*
				The length of plain data used to encrypt depends on the length of your key, 
				it must be not bigger than the number the length of key subtract 11. 
				For example, if your use a 2048bit key, the maximum length is 2048/8 - 11 = 245. 

				2048 = 245
				4096 = 501
				8192 = 1013
				16384 = 2037
				32768 = 4085
			*/

			$bit = 8192;

			$privateKey = openssl_pkey_new(array(
				'private_key_bits' => $bit,      // Size of Key.
				'private_key_type' => OPENSSL_KEYTYPE_RSA,
			));
			// Save the private key to private.key file. Never share this file with anyone.
			openssl_pkey_export_to_file($privateKey, '../keys/private/private-'.$bit.'-'.$currentDateTime.'.key');
			
			// Generate the public key for the private key
			$a_key = openssl_pkey_get_details($privateKey);
			// Save the public key in public.key file. Send this file to anyone who want to send you the encrypted data.
			file_put_contents('../keys/public/public-'.$bit.'-'.$currentDateTime.'.key', $a_key['key']);
			
			// Free the private Key.
			openssl_free_key($privateKey);

		}
	}

	//This script can take quite some time to generate public private keys

	//prevent script from timing out
	set_time_limit(0);

	GenerateKeys::generate();