<?php

	class DBStatement extends PDOStatement {
		public $pdo;
		protected function __construct($pdo) {
			$this->pdo = $pdo;
		}
		function interpolate(array $data=array()) {
			$string=$this->queryString;
			$indexed = $data==array_values($data);
			foreach($data as $k=>$v) {
				if(is_string($v)) $v="'$v'";
				elseif($v===null) $v='NULL';
				if($indexed) $string=preg_replace('/\?/',$v,$string,1);
				else $string=str_replace(":$k",$v,$string);
			}
			return $string;
		}

		function fetchTemplate($template) {
			$rows=[];
			foreach($this as $row)
				$rows[]=preg_replace_callback('/{(.*?)}/',function($matches) use($row) {
					return $row[$matches[1]];
				},$template);
			return $rows;
		}
	}