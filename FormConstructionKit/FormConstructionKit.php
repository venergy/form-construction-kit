<?php

	/**
	 * Form Construction Kit
	 * Repository: https://bitbucket.org/venergy/form-construction-kit/src/master/
	 */

	namespace Form;

	//namespaces
	use Form\Element\Field;
	use Form\Element\Select;
	use Form\Element\Checkbox;
	use Form\Element\Radio;
	use Form\Element\Textarea;
	use Form\Element\Recaptcha;
	use Form\Element\Content;
	use Form\Element\Multifield;
	use Form\Element\Button;
	use Form\Element\Submit;
	use Form\Element\Hiddenfield;
	use Form\Element\FileUpload;
	use Form\Element\Custom;
	use Form\Helpers\FormHelpers;
	use Form\Validators\FormValidators;
	
	//PHPMailer namespaces
	use PHPMailer\PHPMailer\PHPMailer;
	use PHPMailer\PHPMailer\Exception;

	//PDO namespaces
	use \PDO;
	use \PDOException;

	//composer requires
	require dirname(__FILE__).'/../vendor/autoload.php';

	//form construction kit requires
	require_once dirname(__FILE__)."/Element/Field.php";
	require_once dirname(__FILE__)."/Element/Select.php";
	require_once dirname(__FILE__)."/Element/Checkbox.php";
	require_once dirname(__FILE__)."/Element/Radio.php";
	require_once dirname(__FILE__)."/Element/Textarea.php";
	require_once dirname(__FILE__)."/Element/ReCaptcha.php";
	require_once dirname(__FILE__)."/Element/Content.php";
	require_once dirname(__FILE__)."/Element/MultiField.php";
	require_once dirname(__FILE__)."/Element/Button.php";
	require_once dirname(__FILE__)."/Element/Submit.php";
	require_once dirname(__FILE__)."/Element/HiddenField.php";
	require_once dirname(__FILE__)."/Element/FileUpload.php";
	require_once dirname(__FILE__)."/Element/Custom.php";
	require_once dirname(__FILE__)."/FormHelpers.php";
	require_once dirname(__FILE__)."/FormValidators.php";

	//PDO requires
	require dirname(__FILE__).'/DbStatement.php';

	/**
	 * Form Construction Kit class
	 */

	class FormConstructionKit
	{
		//app version
		private $appVersion = 2.21;

		//base url
		private $baseUrl;

		//page url
		private $pageUrl;
		
		//current date time
		private $currentDateTime;

		//unique submission id
		private $uniqueSubmissionId;

		//element count
		private $elementCount = 0;

		//form options
		protected $formOptions;

		//input elements array
		/** @var array $formElements */
		private $formElements = [];

		//output html
		private $html = "";

		//form post data
		private $postData;

		//form file data
		private $fileData;

		//default file upload directory
		private $defaultFileUploadDirectory = "files";

		//has form been submitted
		private $formSubmitted;

		//has form passed all validation?
		private $isValid;

		//number of invalid elements on submission
		private $invalidElementCount = 0;

		//is form using recaptcha
		private $usingRecaptcha = false;

		//subfolder
		private $subfolder;

		//no response element types
		private $noResponseElementTypes = [
			'content'
			,'custom'
			,'button'
			,'submit'
		];

		//non data element types
		private $nonDataElementTypes = [
			'content'
			,'custom'
			,'button'
			,'recaptcha'
			,'submit'
		];

		//utm parameter names
		private $utmParameterNames = ['utm_source','utm_medium','utm_campaign','utm_term','utm_content'];

		//conditional element ids (to hide)
		private $conditionalElementIds = [];

		//serialized data length
		private $serializedDataLength;

		//save data length
		private $saveDataLength;

		public function __construct(Array $formOptions)
		{
			//set base url
			$this->baseUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";

			//set file url
			$this->pageUrl = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

			//set current date time
			$this->currentDateTime = time();

			//set unique submission id (11 numbers)
			$this->uniqueSubmissionId = substr(bin2hex(random_bytes(ceil(11 / 2))), 0, 11);

			//set form's initial validity to false (until proven true)
			$this->isValid = false;

			//set has been submitted flag
			$this->formSubmitted = (count($_POST) > 0) ? true : false;

			//set post object
			$this->postData = $_POST;

			//set file object
			$this->fileData = $_FILES;

			//set object options
			$this->formOptions = $formOptions;

			//set subfolder (root level will return '/')
			$this->subfolder = (strlen(dirname($_SERVER['PHP_SELF'])) > 1) ? dirname($_SERVER['PHP_SELF']) : '';

			//get all url parameters
			$this->formOptions['url-parameters'] = $this->getUrlParameters();

			return $this;
		}

		/**
		 * Add new input to form
		 */

		public function addFormElement(Array $formElement) {

			if(count($formElement) > 0) {

				$this->elementCount++;

				//create element id from label if id is not supplied or generic id if both id and label are not supplied
				if(!isset($formElement['id']) && isset($formElement['label'])) {
					$formElement['id'] = $this->getIdFromLabel($formElement['label']);
				} else if (!isset($formElement['id']) && !isset($formElement['label'])) {
					$formElement['id'] = $formElement['element-type']."-".$this->elementCount;
				}

				//check form element's element-content type and convert all arrays to associative arrays
				if(isset($formElement['element-content']) && !$this->isAssociativeArray($formElement['element-content'])) {
					$formElement['element-content'] = $this->convertToAssociativeArray($formElement['element-content']);
				}

				//check for select initial value and add to element-content associative array
				if($formElement['element-type'] == 'select' && isset($formElement['initial-value'])) {
					$formElement['element-content'] = array($formElement['initial-value'] => $formElement['initial-value']) + $formElement['element-content'];
				}

				//check if using recaptcha
				if($formElement['element-type'] == 'recaptcha'){$this->usingRecaptcha = true;}

				//check for response displays array and add each id to conditionalElementIds array
				if(isset($formElement['response-displays'])) {
					foreach($formElement['response-displays'] AS $responseTrigger => $conditionalElementIds) {
						for($i=0;$i<count($conditionalElementIds);$i++) {
							if(!in_array($conditionalElementIds[$i],$this->conditionalElementIds)) {
								array_push($this->conditionalElementIds,$conditionalElementIds[$i]);
							}
						}
					}
				}

				//set visibility to true
				$formElement['is-visible'] = true;

				//check if this element should be visible
				if(in_array($formElement['id'],$this->conditionalElementIds)) {
					$formElement['is-visible'] = false;
				}
				
				//add to formElements array
				array_push($this->formElements,$formElement);
			}

			return $this;

		}

		/**
		 * Build form html
		 */

		public function build() {

			//set element parents
			$this->assignFormElementsParents();

			//collect form responses
			if($this->formSubmitted) {
				$this->collectResponses();
			}

			//validate this form
			if($this->formSubmitted) {
				$this->validateForm();
			}

			//add utm data if present
			$this->addUtmData();
			
			if(!$this->formSubmitted || ($this->formSubmitted && !$this->isValid)) {

				//initial validation message
				$initialValidationMessage = (isset($this->formOptions['template-variables']['submit-fail-text'])) ? $this->formOptions['template-variables']['submit-fail-text'] : 'Form submission errors. See below.';
				//check if $invalidElementCount is required
				if(strpos($initialValidationMessage, '{% INVALID_ELEMENT_COUNT %}') !== false){$initialValidationMessage = str_replace("{% INVALID_ELEMENT_COUNT %}",$this->invalidElementCount,$initialValidationMessage);}
				//output initial validation message if form is not valid
				$this->html .= ($this->formSubmitted && !$this->isValid) ? "<div class='initial-validation-error'>".$initialValidationMessage."</div>" : '';

				//build form html
				$this->html .= "<form method='post' action='".htmlspecialchars($_SERVER["REQUEST_URI"])."' enctype='multipart/form-data'>";

				for($i=0;$i<count($this->formElements);$i++) {
					$elementType = $this->formElements[$i]['element-type'];
					$this->formElements[$i] = $this->checkForVariableContent_elementOptions($this->formElements[$i]);
					if(method_exists($this,$elementType)){
						$this->$elementType($this->formElements[$i]);
						$this->html .= "<div class='clear-fix'></div>";
					}
				}

				$this->html .= "</form>";

			} else {

				//add success message to output html
				$this->html .= FormHelpers::getHtml($this->checkForVariableContent_submitSuccessHtml($this->getTemplateVariable('submit-success-html')));

				//perform all success actions
				$successActions = (isset($this->formOptions['submit-success-actions'])) ? $this->formOptions['submit-success-actions'] : [];

				//handle all fileupload(s)
				if(count($this->fileData) > 0) {
					//loop through all uploaded files
					foreach($this->fileData AS $key => $value) {
						//get associated formElementId
						$formElementId = $this->getFormElementIndexById($key);

						//get formElement save location
						$fileUploadDirectory = (isset($this->formElements[$formElementId]['file-upload-directory'])) ? $this->formElements[$formElementId]['file-upload-directory'] : $this->defaultFileUploadDirectory;

						//check if directory exists and create directory if it doesn't
						if (!file_exists($fileUploadDirectory)) {
							mkdir($fileUploadDirectory);
						}

						//get filename and format
						$fileName = $this->prepareFileName($value['name']);

						//move file
						move_uploaded_file($_FILES[$key]['tmp_name'], $fileUploadDirectory."/".$fileName);

						//append file location to fileElement response
						$this->formElements[$formElementId]['response'] = $this->baseUrl."/".$fileUploadDirectory."/".$fileName;
					}
				}

				for($i=0;$i<count($successActions);$i++) {

					//standard email
					if(isset($successActions[$i]['type']) && $successActions[$i]['type'] == 'send-email') {
						$this->sendMail($successActions[$i]);
					}

					//save to database
                    if(isset($successActions[$i]['type']) && $successActions[$i]['type'] == 'save-to-database') {

                        //get database credentials
                        $dbHost = $successActions[$i]['database-credentials']['host'];
                        $dbDatabase = $successActions[$i]['database-credentials']['database'];
                        $dbTable = $successActions[$i]['database-credentials']['table'];
                        $dbUsername = $successActions[$i]['database-credentials']['username'];
                        $dbPassword = $successActions[$i]['database-credentials']['password'];
                        $dbPublicKey = $successActions[$i]['database-credentials']['public-key'];
                        $dbExcludeListIds = (isset($successActions[$i]['database-credentials']['exclude-list-ids'])) ? $successActions[$i]['database-credentials']['exclude-list-ids'] : [];

						//save form data to database
                        $this->saveDataToDatabase($dbHost,$dbDatabase,$dbTable,$dbUsername,$dbPassword,$dbPublicKey,$dbExcludeListIds);
                    }

				}

			}

			$this->outputTemplate();

		}

		/**
		 * Assign elements parents
		 */

		private function assignFormElementsParents() {
			//cycle through all elements
			for($i=0;$i<count($this->formElements);$i++) {

				//if element has children
				if(isset($this->formElements[$i]['response-displays'])) {

					//cycle through children
					foreach($this->formElements[$i]['response-displays'] AS $responseTrigger => $conditionalElementIds) {

						//cycle through all conditional element ids
						for($j=0;$j<count($conditionalElementIds);$j++) {

							//cycle through all elements and find parent
							for($k=0;$k<count($this->formElements);$k++) {

								//when parent is found
								if($this->formElements[$k]['id'] == $conditionalElementIds[$j]) {

									//create new element-parents array
									$this->formElements[$k]['element-parents'] = [$this->formElements[$i]['id']];

									//add parent's parents to element-parents array (if they exist)
									if(isset($this->formElements[$i]['element-parents'])) {
										$this->formElements[$k]['element-parents'] = array_merge($this->formElements[$k]['element-parents'],$this->formElements[$i]['element-parents']);
									}
								}
							}
						}
					}
				}
			}
		}

		/**
		 * Collect responses
		 */

		private function collectResponses() {

			for($i=0;$i<count($this->formElements);$i++) {

				$id = (isset($this->formElements[$i]['id'])) ? $this->formElements[$i]['id'] : '';
				$elementType = $this->formElements[$i]['element-type'];

				//set element values
				if ($elementType == 'recaptcha') {

					//get g-recaptcha  value
					$this->formElements[$i]['g-recaptcha-response'] = (!empty($this->postData['g-recaptcha-response'])) ? $this->postData['g-recaptcha-response'] : '';

				} else if ($elementType == 'fileupload') {

					//get file upload data
					if(isset($this->fileData[$this->formElements[$i]['id']])) {
						$this->formElements[$i]['response'] = $this->fileData[$this->formElements[$i]['id']]['name'];
						$this->formElements[$i]['file-type'] = $this->fileData[$this->formElements[$i]['id']]['type'];
						$this->formElements[$i]['file-size'] = $this->fileData[$this->formElements[$i]['id']]['size'];
					} else {
						$this->formElements[$i]['response'] = '';
					}

				} else if (in_array($elementType,$this->noResponseElementTypes)) {

					//do nothing

				} else if ($this->formElements[$i]['is-visible']) {

					//get single value
					$this->formElements[$i]['response'] = (isset($this->postData[$id]) && !empty($this->postData[$id])) ? $this->sanitizeInput($this->postData[$id]) : '';

					//set triggered child element's visiblitly to true
					if(isset($this->formElements[$i]['response-displays'])) {
						foreach($this->formElements[$i]['response-displays'] AS $responseTrigger => $conditionalElementId) {
							if($responseTrigger == $this->formElements[$i]['response'] || (gettype($this->formElements[$i]['response']) == 'array' && in_array($responseTrigger,$this->formElements[$i]['response']))) {
								//remove element id from conditionalElementIds array
								if (($veKey = array_search($conditionalElementId, $this->conditionalElementIds)) !== false) {
									unset($this->conditionalElementIds[$veKey]);
								}
								//set element's visiblity
								$this->setElementVisabilityById($conditionalElementId,true);
							}
						}
					}

				}
			}
		}

		/**
		 * Validate
		 */

		private function validateForm() {

			//set form validity to true and prove false via validation
			$this->isValid = true;

			$formValidators = new FormValidators();

			for($i=0;$i<count($this->formElements);$i++) {

				$hasValidators = (isset($this->formElements[$i]['validation']) && count($this->formElements[$i]['validation']) > 0) ? true : false;
				$isVisable = $this->formElements[$i]['is-visible'];
				$this->formElements[$i]['is-valid'] = true;
				
				if($hasValidators && $isVisable) {

					//element has validation requirements - validate (regulate!)

					$isMultiField = ($this->formElements[$i]['element-type'] == 'multifield') ? true : false;

					$response = (isset($this->formElements[$i]['response'])) ? $this->formElements[$i]['response'] : '';

					$this->formElements[$i]['validation-message'] = '';

					foreach($this->formElements[$i]['validation'] AS $validator => $validationMessage) {

						//handle min/max chars validation
						if(substr($validator,0,9) == 'min-chars') {
							$validatorLimit = explode(":", $validator)[1];
							$validator = 'min-chars';
						}

						if(substr($validator,0,9) == 'max-chars') {
							$validatorLimit = explode(":", $validator)[1];
							$validator = 'max-chars';
						}

						//handle fileupload file size validation
						if(substr($validator,0,18) == 'restrict-file-size') {
							$validatorExplode = explode(":", $validator);
							$validatorLimit = end($validatorExplode);
							$validator = 'restrict-file-size';
						}

						//handle match-input validation
						if(substr($validator,0,11) == 'match-input') {
							$validatorExplode = explode(":", $validator);
							$targetResponse = $this->getElementResponse(end($validatorExplode));
							$validator = 'match-input';
						}

						switch ($validator) {
							case 'not-empty':
								$elementValid = $formValidators->notEmpty($response,$isMultiField);
								break;

							case 'not-first-select-value':
								$firstKey = $this->array_key_first($this->formElements[$i]['element-content']);
								$elementValid = $formValidators->notFirstSelect($response,$this->formElements[$i]['element-content'][$firstKey]);
								break;

							case 'alpha-only':
								$elementValid = $formValidators->alphaOnly($response,$isMultiField);
								break;

							case 'numeric-only':
								$elementValid = $formValidators->numericOnly($response,$isMultiField);
								break;

							case 'alpha-numeric-only':
								$elementValid = $formValidators->alphaNumericOnly($response,$isMultiField);
								break;

							case 'valid-email':
								$elementValid = $formValidators->validEmail($response,$isMultiField);
								break;

							case 'match-input':
								$elementValid = $formValidators->matchInput($response,$targetResponse);
								break;

							case 'valid-url':
								$elementValid = $formValidators->validUrl($response,$isMultiField);
								break;

							case 'min-chars':
								$elementValid = $formValidators->minChars($response,$validatorLimit,$isMultiField);
								break;

							case 'max-chars':
								$elementValid = $formValidators->maxChars($response,$validatorLimit,$isMultiField);
								break;

							case 'valid-recaptcha':
								$gRecaptchaSecret = $this->formElements[$i]['g-recaptcha-secret'];
								$elementValid = $formValidators->validRecaptcha($this->formElements[$i]['g-recaptcha-response'],$gRecaptchaSecret);
								break;
							
							case 'only-accepted-file-types':
								$elementValid = $formValidators->validFileType($response,$this->formElements[$i]['accepted-file-types']);
								break;

							case 'restrict-file-size':
								$elementValid = $formValidators->validFileSize($response,$this->formElements[$i]['file-size'],$validatorLimit);
								break;
							
							default:
								$elementValid = true;
								break;
						}
						
						if(!$elementValid){

							//update invalidCount
							$this->invalidElementCount++;

							//only set element's validity if false
							$this->formElements[$i]['is-valid'] = $elementValid;

							//if any validation fails set isValid to false at a class level
							$this->isValid = false;

							//if validation fails then add validation message to form element
							$this->formElements[$i]['validation-message'] .= $validationMessage;

							//if first validator fails (is not valid) then skip next element validators
							break;
						}
					}
				}
			}
		}

		/**
		 * Form elements
		 */

		private function field(Array $elementData) {

			$elementField = new Field($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementField->getHTML();
		}

		private function select(Array $elementData) {

			$elementSelect = new Select($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementSelect->getHTML();

		}

		private function checkbox(Array $elementData) {

			$elementCheckbox = new Checkbox($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementCheckbox->getHTML();
		}

		private function radio(Array $elementData) {

			$elementRadio = new Radio($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementRadio->getHTML();
		}

		private function textarea(Array $elementData) {

			$elementTextarea = new Textarea($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementTextarea->getHTML();
		}

		private function recaptcha(Array $elementData) {

			$elementRecaptcha = new Recaptcha($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementRecaptcha->getHTML();

		}

		private function content(Array $elementData) {

			$elementContent = new Content($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementContent->getHTML();
		}

		private function multifield(Array $elementData) {

			$elementMultifield = new Multifield($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementMultifield->getHTML();
		}

		private function button(Array $elementData) {

			$elementButton = new Button($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementButton->getHTML();
		}

		private function submit(Array $elementData) {

			$elementSubmit = new Submit($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementSubmit->getHTML();
		}

		private function hiddenfield(Array $elementData) {

			$elementHiddenfield = new Hiddenfield($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementHiddenfield->getHTML();
		}

		private function fileupload(Array $elementData) {

			$elementFileupload = new FileUpload($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementFileupload->getHTML();
		}

		private function custom(Array $elementData) {

			$elementCustom = new Custom($elementData,$this->postData,$this->formSubmitted);

			$this->html .= $elementCustom->getHTML();
		}

		/**
		 * Send mail
		 */

		private function sendMail(Array $successAction) {

			if(isset($successAction['email-template'])){

				//get email content
				$emailContentTemplate = $successAction['email-template']['content'];

				//load email content and related variables (emailTo,emailSubject,emailFrom,emailFromName,emailContentHTML)
				require_once "email-template/content/".$emailContentTemplate.".php";

				//get email containter template
				$emailContainerTemplate = $successAction['email-template']['container'];
				$emailTemplateHTML = file_get_contents("email-template/container/".$emailContainerTemplate.".php");

				//inject header title
				$emailTemplateHTML = str_replace('{% TITLE %}',$emailSubject,$emailTemplateHTML);

				//inject content
				$emailTemplateHTML = str_replace('{% BODY %}',$emailContentHTML,$emailTemplateHTML);

				//inject image url base (subfolder,email template)
				$imageUrlBase = $this->getImgUrlBase();
				$emailTemplateHTML = str_replace('{% IMGURLBASE %}',$imageUrlBase,$emailTemplateHTML);

				//convert html content to plain text for AltBody
				$emailContentText = $this->convertHtmlToText($emailContentHTML);

				//send email
				$mail = new PHPMailer(true);

				//test email recipient / sender (override)
				// $emailTo = ["*****@*****.***"];
				// $emailFrom = "*****@*****.***";

				try{
					
					$mail->isHTML(true); //set email format to HTML

					//recipients (requires arrays)
					for($i=0;$i<count($emailTo);$i++){$mail->addAddress($emailTo[$i]);}
					if(isset($emailToCC)){for($i=0;$i<count($emailToCC);$i++){$mail->addCC($emailToCC[$i]);}}
					if(isset($emailToBCC)){for($i=0;$i<count($emailToBCC);$i++){$mail->addBCC($emailToBCC[$i]);}}

					//sender
					$mail->setFrom($emailFrom,$emailFromName);
					$mail->addReplyTo($emailFrom, $emailFromName);

					//dkim
					if(isset($dkimOn) && $dkimOn){
						if(isset($dkimDomain)){$mail->DKIM_domain = $dkimDomain;}
						if(isset($dkimPrivateKey)){$mail->DKIM_private = $dkimPrivateKey;}
						if(isset($dkimSelector)){$mail->DKIM_selector = $dkimSelector;}
						if(isset($dkimPassphrase)){$mail->DKIM_passphrase = $dkimPassphrase;} else {$mail->DKIM_passphrase = '';}
						if(isset($dkimIdentity)){$mail->DKIM_identity = $dkimIdentity;} else {$mail->DKIM_identity = $mail->From;}
						if(isset($dkimHeaderFields)){$mail->DKIM_copyHeaderFields = $dkimHeaderFields;} else {$dkimHeaderFields = false;}
						if(isset($dkimExtraHeaders)){$mail->DKIM_extraHeaders = $dkimExtraHeaders;}
					}

					//content
					$mail->Subject = $emailSubject;
					$mail->Body = $emailTemplateHTML; //html
					$mail->AltBody = $emailContentText; //text

					//send
					$mail->send();

					//test email output
					//echo $emailTemplateHTML;

				} catch (Exception $e) {
					$this->html .= "An error occured sending mail: ".$e;
				}
			}
		}

		/**
		 * Helper methods
		 */

		public function isValid() {
			return $this->isValid;
		}

		public function getFormData() {
			return $this->formElements;
		}

		private function getIdFromLabel(String $label) {

			$label = preg_replace('/[^A-Za-z0-9\ -]/', '', $label); // Removes special chars.
			$label = strtolower($label); //string to all lowercase
			return str_replace(" ","-",$label); //replace spaces with dashes
		}

		private function getElementResponse(String $elementLabel) {
			//get element id
			$fieldId = $this->getIdFromLabel($elementLabel);
			//get element value
			for($i=0;$i<count($this->formElements);$i++) {
				if(isset($this->formElements[$i]['id']) && $fieldId == $this->formElements[$i]['id']) {
					return $this->formElements[$i]['response'];
				}
			}
		}

		private function getStructuredFormContent(Bool $excludeUtmData = false, Array $excludeListIds = []) {

			$content = "";

			for($i=0;$i<count($this->formElements);$i++) {

				//include/exclude utm data
				if(substr($this->formElements[$i]['id'],0,3) == 'utm' && $excludeUtmData){continue;}

				//exclude element results from exclude list
				if(in_array($this->formElements[$i]['id'],$excludeListIds)){continue;}

				$elementLabel = (isset($this->formElements[$i]['label'])) ? $this->formElements[$i]['label'] : $this->formElements[$i]['id'];

				if(isset($this->formElements[$i]['response']) && gettype($this->formElements[$i]['response']) == 'array') {
					$content .= $elementLabel.": <br/>";

					foreach($this->formElements[$i]['response'] AS $key => $value) {
						$content .= nl2br($value)."<br/>";
					}

					$content .= "<br/>";

				} else if (isset($this->formElements[$i]['response'])) {
					$content .= $elementLabel.": <br/>".nl2br($this->formElements[$i]['response'])."<br/><br/>";
				}

			}

			return $content;
			
		}

		private function getFormResultsData(Array $excludeListIds = []) {
			
			$resultsData = [];

			for($i=0;$i<count($this->formElements);$i++) {

				//exclude all non data element types
				if(in_array($this->formElements[$i]['element-type'],$this->nonDataElementTypes)) {continue;}

				//exclude element results from exclude list
				if(in_array($this->formElements[$i]['id'],$excludeListIds)){continue;}

				if(isset($this->formElements[$i]['response']) && gettype($this->formElements[$i]['response']) == 'array') {

					$multiResponse = "";
					$multiResponseIndex = 0;

					foreach($this->formElements[$i]['response'] AS $key => $value) {
						if($multiResponseIndex > 0){$multiResponse .= ',';}
						$multiResponse .= $value;
						$multiResponseIndex++;
					}

					$resultsData[$this->formElements[$i]['id']] = $multiResponse;

				} else {
					$resultsData[$this->formElements[$i]['id']] = (isset($this->formElements[$i]['response'])) ? $this->formElements[$i]['response'] : '';
				}

			}

			return $resultsData;
		}

		//polyfill for lower versions than PHP 7.3

		private function array_key_first(Array $array) {
			return key(array_slice($array, 0, 1));
		}

		protected function getCSS() {
			$css = "";

			//if loading additional js then load jquery ui styles
			if(isset($this->formOptions['js']) && count($this->formOptions['js']) > 0) {
				$css .= "<link rel='stylesheet' href='https://code.jquery.com/ui/1.13.2/themes/base/jquery-ui.css' integrity='sha256-yMIVeRjJ/tC7ncxWyWtS3Hr3CwXKAijkZ+r5F3d1Gtc=' crossorigin='anonymous'>";
			}

			if(isset($this->formOptions['css']) && count($this->formOptions['css']) > 0) {

				$formVersion = (isset($this->formOptions['form-version'])) ? ".".$this->formOptions['form-version'] : '';
				
				for($i=0;$i<count($this->formOptions['css']);$i++) {
					$css .= "<link rel='stylesheet' href='css/".$this->formOptions['css'][$i].".css?av=".$this->appVersion.$formVersion."'>";
				}
			}

			return $css;
		}

		protected function getJS() {

			$js = "";

			//if using recaptcha include the recaptcha api
			if($this->usingRecaptcha) {
				$js .= "<script src='https://www.google.com/recaptcha/api.js'></script>";
			}

			if(isset($this->formOptions['js']) && count($this->formOptions['js']) > 0) {

				//if loading additional js load jquery libraries
				$js .= "<script src='https://code.jquery.com/jquery-3.7.1.min.js' integrity='sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=' crossorigin='anonymous'></script>";
				$js .= "<script src='https://code.jquery.com/ui/1.13.2/jquery-ui.min.js' integrity='sha256-lSjKY0/srUM9BE3dPm+c4fBo1dky2v27Gdjm2uoZaL0=' crossorigin='anonymous'></script>";

				$formVersion = (isset($this->formOptions['form-version'])) ? ".".$this->formOptions['form-version'] : '';

				for($i=0;$i<count($this->formOptions['js']);$i++) {
					$js .= "<script src='js/".$this->formOptions['js'][$i].".js?av=".$this->appVersion.$formVersion."'></script>";
				}
			}
			return $js;
		}

		protected function getGtmTag($section) {
			if(isset($this->formOptions['template-variables']['google-tag-manager-tracking-id'])) {

				$gtmtid = $this->formOptions['template-variables']['google-tag-manager-tracking-id'];

				if($section == 'head') {

					return "<!-- Google Tag Manager -->
					<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
					new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
					j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
					'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
					})(window,document,'script','dataLayer','".$gtmtid."');</script>
					<!-- End Google Tag Manager -->";

				} else if ($section == 'body') {
					return "<!-- Google Tag Manager (noscript) -->
					<noscript><iframe src='https://www.googletagmanager.com/ns.html?id=".$gtmtid."'
					height='0' width='0' style='display:none;visibility:hidden'></iframe></noscript>
					<!-- End Google Tag Manager (noscript) -->";
				}

				return '';

			}
		}

		protected function getGaTag() {

			if(isset($this->formOptions['template-variables']['google-analytics-tracking-id'])) {

				$gatid = $this->formOptions['template-variables']['google-analytics-tracking-id'];

				return "<!-- Global site tag (gtag.js) - Google Analytics -->
				<script async src='https://www.googletagmanager.com/gtag/js?id=".$gatid."'></script>
				<script>
				window.dataLayer = window.dataLayer || [];
				function gtag(){dataLayer.push(arguments);} 
				gtag('js', new Date());
				gtag('config', '".$gatid."');
				</script>";
			}

		}

		protected function getTemplateVariable(String $pageVariableName) {
			return (isset($this->formOptions['template-variables'][$pageVariableName])) ? $this->formOptions['template-variables'][$pageVariableName] : '';
		}

		protected function getTemplateVariableHTML(String $pageVariableName) {
			return (isset($this->formOptions['template-variables'][$pageVariableName])) ? FormHelpers::getHtml($this->formOptions['template-variables'][$pageVariableName]) : '';
		}

		protected function getFormOption(String $formOptionName) {
			return (isset($this->formOptions[$formOptionName])) ? $this->formOptions[$formOptionName] : '';
		}

		private function outputTemplate() {
			return require_once "form-template/".$this->formOptions['form-template'].".php";
		}

		private function sanitizeInput($input) {

			if(gettype($input) == 'array') {
				for($i=0;$i<count($input);$i++) {
					$input[$i] = htmlspecialchars($input[$i]);
				}
			} else {
				$input = htmlspecialchars($input);
			}
			return $input;
		}

		private function isAssociativeArray(Array $array)
		{
			if (array() === $array) return false;
			return array_keys($array) !== range(0, count($array) - 1);
		}

		private function convertToAssociativeArray(Array $array){
			
			$associativeArray = [];
			
			for($i=0;$i<count($array);$i++) {
				$associativeArray["$array[$i]"] = $array[$i];
			}

			return $associativeArray;
		}

		private function addUtmData() {

			//adds new formElements for utm params fournd in url
			$utmParameterNames = $this->utmParameterNames;

			for($i=0;$i<count($utmParameterNames);$i++) {

				$utmResponse = "";

				if(isset($_GET[$utmParameterNames[$i]])) { 
					$utmResponse = $_GET[$utmParameterNames[$i]];
				} 

				$utmData = ['id'=>$utmParameterNames[$i],'element-type'=>'hiddenfield','response'=>$this->sanitizeInput($utmResponse)];
				array_push($this->formElements,$utmData);

			}

		}

		private function saveDataToDatabase(String $host, String $database, String $table, String $username, String $password, String $pubKey, Array $excludeListIds = []){

			//prepare PDO
			$dsn = 'mysql:dbname='.$database.';host='.$host;
			
			//new php database object (pdo)
			try {
				$pdo = new PDO($dsn,$username,$password);
			}
			catch(PDOException $e) {
				die('Database connection error.');
			}

			//MySQL
			$pdo->exec('SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;');
			$pdo->exec('SET SESSION sql_mode = \'ANSI\';');

			//Development
			$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

			//True Prepared Statements
			$pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES,false);

			//Extend PDOStatement
			$pdo->setAttribute(PDO::ATTR_STATEMENT_CLASS, array('DBStatement',array($pdo)));

			//prepare data
			//retrieve associative array of id=>result
			$dataArray = $this->getFormResultsData($excludeListIds);

			//serialize data
			$serializedData = serialize($dataArray);

			//check data string length for a 8192bit key. max chars = 1013 (explained in /FormConstructionKit/GenerateKeys.php)
			$this->serializedDataLength = strlen($serializedData);

			if(file_exists('keys/public/'.$pubKey.'.key')) {
				//get public key
				$publicKey = file_get_contents('keys/public/'.$pubKey.'.key');

				//encrypt data using public key
				openssl_public_encrypt($serializedData, $encryptedData, $publicKey);

				// Use base64_encode to make contents viewable/sharable
				$saveData = base64_encode($encryptedData);
			} else {
				$saveData = $serializedData;
			}

			//check saving string length
			$this->saveDataLength = strlen($saveData);

			//save to database
			$query = "INSERT INTO ".$table." (date,uid,data) VALUES (?,?,?)";
			$prepared = $pdo->prepare($query);
			$prepared->execute([$this->currentDateTime,$this->uniqueSubmissionId,$saveData]);
			
		}

		private function getImgUrlBase(){

			$urlBase = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? 'https' : 'http';
			$urlBase .= "://".$_SERVER["SERVER_NAME"].$this->subfolder."/img";

			return $urlBase;
		}

		private function convertHtmlToText(String $html) {

			//convert all line breaks
			$plainText = str_replace("<br/>", "\n" , $html);

			//fix ul & li spacing for text email
			$plainText = str_replace("<ul>", "\n" , $plainText);
			$plainText = str_replace("</ul>", "\n" , $plainText);
			$plainText = str_replace("<li>", "\n- " , $plainText);
			$plainText = str_replace("</li>", "\n" , $plainText);

			//remove all links
			$plainText = preg_replace('/<a href=[\'"]([a-zA-Z0-9:\/\.\-_%]+)[\'"]>([a-zA-Z0-9 ]+)<\/a>/','$2 ($1)', $plainText);

			//remove all tags
			$plainText = strip_tags($plainText);

			return $plainText;
		}

		private function setElementVisabilityById(Array $elementIds, Bool $isVisable) {
			for($i=0;$i<count($elementIds);$i++) {
				for($j=0;$j<count($this->formElements);$j++) {
					if($this->formElements[$j]['id'] == $elementIds[$i]) {
						$this->formElements[$j]['is-visible'] = $isVisable;
					}
				}
			}
		}

		private function getEmailVerificationUrl() {

			$verificationUrl = (isset($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") ? 'https' : 'http';
			$verificationUrl .= "://".$_SERVER["SERVER_NAME"].$this->subfolder."/".basename($_SERVER["SCRIPT_FILENAME"], '.php')."-validate.php?sdt=".$this->currentDateTime."&suid=".$this->uniqueSubmissionId;
				
			return $verificationUrl;
		}

		protected function getSerializedDataLength() {
			return (!empty($this->serializedDataLength)) ? $this->serializedDataLength : '';
		}

		protected function getSaveDataLength() {
			return (!empty($this->saveDataLength)) ? $this->saveDataLength : '';
		}

		private function getFormElementIndexById($id){
			$formElementId = '';
			for($i=0;$i<count($this->formElements);$i++) {
				if($this->formElements[$i]['id'] == $id) {
					$formElementId = $i;
				}
			}
			return $formElementId;
		}

		private function prepareFileName($filename) {
			$filename = str_replace(' ','_',$filename);
			$filename = preg_replace("/[^a-zA-Z0-9_.]+/", "", $filename);
			$filename = str_replace('__','_',$filename);
			$filename = $this->uniqueSubmissionId . "_" . $filename;
			return $filename;
		}

		private function getUrlParameters() {
			$queryString = $_SERVER['QUERY_STRING'];
			if(!empty($queryString)) {
				parse_str($queryString,$queryStringOutput);
				return $queryStringOutput;
			} else {
				return [];
			}
		}

		private function checkForVariableContent_elementOptions(array $formElementOptions) {
			foreach($formElementOptions AS $elementName => $elementValue) {
				switch ($elementName) {
					case 'html-content':
						for($i=0;$i<count($elementValue);$i++){
							$formElementOptions[$elementName][$i][1] = $this->replaceVariableContent($elementValue[$i][1]);
						}
						break;
					case 'description-text':
						$formElementOptions[$elementName] = $this->replaceVariableContent($elementValue);
						break;
					case 'placeholder':
						$formElementOptions[$elementName] = $this->replaceVariableContent($elementValue);
						break;
					case 'validation-message':
						$formElementOptions[$elementName] = $this->replaceVariableContent($elementValue);
						break;
					default:
						break;
				}
			}
			return $formElementOptions;
		}

		private function checkForVariableContent_submitSuccessHtml(array $submitSuccessHtml) {
			for($i=0;$i<count($submitSuccessHtml);$i++){
				$submitSuccessHtml[$i][1] = $this->replaceVariableContent($submitSuccessHtml[$i][1]);
			}
			return $submitSuccessHtml;
		}

		private function replaceVariableContent(string $content) {

			preg_match_all("/{{([^}]+)}}/", $content, $matches);
			
			$matches;
			$urlParameters = $this->formOptions['url-parameters'];

			if(!empty($matches[0])) {
				for($i=0;$i<count($matches[0]);$i++) {

					//check for variable with fallback value {{name}} or {{name|Joe}}
					$matchData = explode('|',$matches[1][$i]);

					//has fallback value
					if(count($matchData) > 1) {
						if(array_key_exists($matchData[0], $urlParameters)){
							$content = str_replace($matches[0][$i],$this->sanitizeInput($urlParameters[$matchData[0]]),$content);
						} else {
							$content = str_replace($matches[0][$i],$this->sanitizeInput($matchData[1]),$content);
						}
					} else {
						//does not have fallback value
						if(array_key_exists($matches[1][$i], $urlParameters)){
							$content = str_replace($matches[0][$i],$this->sanitizeInput($urlParameters[$matches[1][$i]]),$content);
						} else {
							//$content = str_replace($matches[0][$i],$this->sanitizeInput($matches[1][$i]),$content);
						}
					}
				}
			} 
			return $content;
		}

	}
	