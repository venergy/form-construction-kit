<?php

	//show all errors
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	/**
	 * LOAD FORM CONSTRUCTION KIT
	 */

	use Form\FormConstructionKit;
	require_once "FormConstructionKit/FormConstructionKit.php";

	require_once "lib/countries.php";

	/**
	 * INSTANTIATE NEW FORM OBJECT
	 */

	$form = new FormConstructionKit([
		//form options
		'form-template'=>"blank"
		,'css'=>['bootstrap.min','global']
		,'js'=>['bootstrap.bundle.min','global']
		,'template-variables'=>[
			'meta-title'=>"Conditional Elements Demo | Form Construction Kit"
			,'google-analytics-tracking-id'=>"**-********-*"
			,'google-tag-manager-tracking-id'=>"***-*******"
			,'header-text'=>"Form Construction Kit Conditional Elements Demo"
			,'submit-fail-text'=>"There are some errors with your submission. Please see below."
			,'submit-success-html'=>[
				['h2',"Success"]
				,['p',"Thank you for your submission. We have sent you an email."]
			]
		]
		,'submit-success-actions'=>[
			[
				'type'=>"send-email"
				,'email-template'=>[
					'container'=>"blank"
					,'content'=>"demo"
					,'email-to'=>["recipient@email.com"]
					// ,'email-to-cc'=>["recipient@email.com"]
					// ,'email-to-bcc'=>["recipient@email.com"]
					,'email-subject'=>"Subject"
					,'email-from'=>"sender@email.com"
					,'email-from-name'=>"Mr Email Sender"
				]
			],
			// [
            //     'type'=>'save-to-database'
            //     ,'database-credentials'=>[
            //         'host'=>'127.0.0.1'
            //         ,'database'=>'db_name'
            //         ,'table'=>'tb_name'
            //         ,'username'=>'root'
            //         ,'password'=>'root'
            //         ,'public-key'=>'public-8192-1618221780'
            //         ,'exclude-list-ids'=>['id-to-ignore']
            //     ]
            // ]
		]
	]);

	/**
	 * ADD FORM ELEMENTS
	 */

	 //LEVEL 1 -----

	$form->addFormElement([
		'label'=>"Have you seen the conditional elements in form construction kit?"
		,'element-type'=>'radio'
		,'element-content'=>[
			"Yes"=>'yes'
			,"No"=>'no'
		]
		,'validation'=>[
			'not-empty'=>"Select an option"
		]
		,'response-displays'=>[
			'yes'=>['what-do-you-think-of-this-new-feature']
			,'no'=>['oh-ok-can-i-show-you-around']
		]
	]);

	//LEVEL 2 -----

	$form->addFormElement([
		'label'=>"What do you think of this new feature?"
		,'id'=>"what-do-you-think-of-this-new-feature"
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	$form->addFormElement([
		'label'=>"Oh, ok. Can I show you around?"
		,'id'=>"oh-ok-can-i-show-you-around"
		,'element-type'=>'radio'
		,'element-content'=>[
			'Sure thing'=>'sure-thing'
			,"No thanks. I don't have time"=>'no-thanks-i-dont-have-time'
		]
		,'validation'=>[
			'not-empty'=>"Select an option"
		]
		,'response-displays'=>[
			'sure-thing'=>['lets-start-with-checkboxes']
			,'no-thanks-i-dont-have-time'=>['why-dont-you-have-any-time']
		]
	]);

	//LEVEL 3 -----

	$form->addFormElement([
		'label'=>"Why don't you have any time?"
		,'id'=>"why-dont-you-have-any-time"
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	$form->addFormElement([
		'label'=>"Let's start with checkboxes."
		,'description-text'=>"You can select more than one checkbox"
		,'id'=>"lets-start-with-checkboxes"
		,'element-type'=>'checkbox'
		,'element-content'=>[
			'Show me radio buttons'=>'show-me-radio-buttons'
			,'Show me a select dropdown'=>"show-me-a-select-dropdown"
			,'Show me a textarea'=>"show-me-a-textarea"
			,'Show me some fields'=>"show-me-some-fields"
		]
		,'validation'=>[
			'not-empty'=>"Select an option"
		]
		,'response-displays'=>[
			'show-me-radio-buttons'=>['these-are-radio-buttons']
			,'show-me-a-select-dropdown'=>['this-is-a-select-dropdown']
			,'show-me-a-textarea'=>['this-is-a-textarea']
			,'show-me-some-fields'=>['this-is-a-field','this-is-another-field','here-are-some-extra-radio-buttons']
		]
	]);

	//LEVEL 4 -----

	$form->addFormElement([
		'label'=>"These are radio buttons"
		,'id'=>'these-are-radio-buttons'
		,'element-type'=>'radio'
		,'element-content'=>[
			"Radio 1"=>'radio-1'
			,"Radio 2"=>'radio-2'
			,"Radio 3"=>'radio-3'
		]
		,'validation'=>[
			'not-empty'=>"Select an option"
		]
		,'response-displays'=>[
			'radio-1'=>['radio-1-description']
			,'radio-2'=>['radio-2-content']
		]
	]);

	//LEVEL 4a

	$form->addFormElement([
		'label'=>"Radio 1 description"
		,'id'=>'radio-1-description'
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	$form->addFormElement([
		'id'=>'radio-2-content'
		,'element-type'=>'content'
		,'html-content'=>[
			['h3',"Radio 2 description",'no-top-margin']
			,['p',"Id quis in cillum ipsum ad laboris enim velit eiusmod aute. <a href='https://google.com' target='_blank'>hello world</a>"]
			,['p',"Elit commodo dolor dolor mollit incididunt do officia occaecat ea magna elit officia sit consectetur."]
		]
	]);

	//LEVEL 4 (cont.)

	$form->addFormElement([
		'label'=>"This is a select dropdown"
		,'id'=>'this-is-a-select-dropdown'
		,'element-type'=>'select'
		,'initial-value'=>"Please select"
		,'element-content'=>[
			"Select 1"=>'select-1'
			,"Select 2"=>'select-2'
		]
		,'validation'=>[
			'not-first-select-value'=>"Select an option"
		]
		,'response-displays'=>[
			'select-1'=>['select-1-description']
		]
	]);

	//LEVEL 4b

	$form->addFormElement([
		'label'=>"Select 1 description"
		,'id'=>'select-1-description'
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	//LEVEL 4 (cont.)

	$form->addFormElement([
		'label'=>"This is a text area"
		,'id'=>'this-is-a-textarea'
		,'element-type'=>'textarea'
		,'validation'=>[
			'not-empty'=>"Textarea can not be empty"
		]
	]);

	$form->addFormElement([
		'label'=>"This is a field (1 of 2)"
		,'id'=>'this-is-a-field'
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	$form->addFormElement([
		'label'=>"This is another field (2 of 2)"
		,'id'=>'this-is-another-field'
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	$form->addFormElement([
		'label'=>"And here are some extra radio buttons"
		,'id'=>"here-are-some-extra-radio-buttons"
		,'element-type'=>'radio'
		,'element-content'=>[
			'Radio A'=>'radio-a'
			,"Radio B"=>'radio-b'
		]
		,'validation'=>[
			'not-empty'=>"Select an option"
		]
		,'response-displays'=>[
			'radio-a'=>['radio-a-html']
			,'radio-b'=>['radio-b-description']
		]
	]);

	//LEVEL 5

	$form->addFormElement([
		'element-type'=>'custom'
		,'id'=>'radio-a-html'
		,'code'=>"<img style='width:100px;margin:20px 0 35px;' src='https://media.newyorker.com/photos/59095bb86552fa0be682d9d0/master/w_2240,c_limit/Monkey-Selfie.jpg'/>"
	]);

	$form->addFormElement([
		'label'=>"Radio B description"
		,'id'=>'radio-b-description'
		,'element-type'=>'field'
		,'validation'=>[
			'not-empty'=>"Field can not be empty"
		]
	]);

	//END -----

	//submit
	$form->addFormElement([
		'label'=>"Submit"
		,'element-type'=>"submit"
	]);

	//content (with defined class)
	$form->addFormElement([
		'element-type'=>'content'
		,'html-content'=>[
			['p',"Please note that all fields marked with an asterisk (*) are mandatory.",'footnote']
		]
	]);

	/**
	 * PROCESS FORM & OUTPUT VALIDATED HTML/CSS/JS
	 */

	$form->build();
