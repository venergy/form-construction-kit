<?php

	use Form\EmailValidation;

	require_once "FormConstructionKit/EmailValidation.php";

	$emailValidation = (new EmailValidation([
		'form-template'=>"blank"
		,'css'=>['global']
		,'js'=>['global']
		,'template-variables'=>[
			'meta-title'=>"Elements Demo | Form Construction Kit"
			,'google-analytics-tracking-id'=>"**-********-*"
			,'google-tag-manager-tracking-id'=>"***-*******"
			,'header-text'=>"Form Construction Kit Email Validate Demo"
			,'validation-success-html'=>[
				['h2',"Email confirmed"]
				,['p',"Thank you. Your email has been confirmed."]
			]
			,'validation-fail-html'=>[
				['h2',"Error"]
				,['p',"A system error has occured. Please contact the administrator."]
			]
		]
		,'database-credentials'=>[
			'host'=>'127.0.0.1'
			,'database'=>'db_name'
			,'table'=>'tb_name'
			,'username'=>'root'
			,'password'=>'root'
		]
	]))->validate();