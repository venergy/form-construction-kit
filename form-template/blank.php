<!DOCTYPE html>
<html lang="en">

	<head>

		<!-- output GTM head tag if exists in config -->
		<?= $this->getGtmTag('head'); ?>

		<!-- output GA tag if exists in config -->
		<?= $this->getGaTag(); ?>

		<meta charset="UTF-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title><?= $this->getTemplateVariable('meta-title'); ?></title>

		<!-- output page css -->
		<?= $this->getCSS(); ?>

	</head>

	<body>

		<!-- output GTM body tag if exists in config -->
		<?= $this->getGtmTag('body'); ?>
		
		<div class="content-container">

			<h1><?= $this->getTemplateVariable('header-text'); ?></h1>

			<!-- output form content -->
			<?= $this->html; ?>

			<!-- output serialised data length -->
			<div class="footnote"><?= $this->getSerializedDataLength(); ?> <?= $this->getSaveDataLength(); ?></div>
			
		</div>

		<!-- output page js -->
		<?= $this->getJS(); ?>

	</body>
</html>