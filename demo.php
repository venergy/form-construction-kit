<?php

	//show all errors
	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	/**
	 * LOAD FORM CONSTRUCTION KIT
	 */

	use Form\FormConstructionKit;
	require_once "FormConstructionKit/FormConstructionKit.php";

	require_once "lib/countries.php";

	/**
	 * INSTANTIATE NEW FORM OBJECT
	 * form-template: 
	 * css:
	 * js:
	 * template-variables:
	 * 		submit-fail-text:
	 * 		submit-success-html:
	 * submit-success-actions:
	 * 		type: (send-email,save-to-database)
	 */

	$form = new FormConstructionKit([
		//form options
		'form-template'=>"blank"
		,'css'=>['bootstrap.min','global']
		,'js'=>['bootstrap.bundle.min','global','countries','demo']
		,'template-variables'=>[
			'meta-title'=>"Elements Demo | Form Construction Kit"
			,'google-analytics-tracking-id'=>"**-********-*"
			,'google-tag-manager-tracking-id'=>"***-*******"
			,'header-text'=>"Form Construction Kit Demo"
			,'submit-fail-text'=>"There are {% INVALID_ELEMENT_COUNT %} errors with your submission. Please see below."
			,'submit-success-html'=>[
				['h2',"Success"]
				,['p',"Thank you for your submission. We have sent you a confirmation email."]
			]
		]
		,'submit-success-actions'=>[
			[
				'type'=>"send-email"
				,'email-template'=>[
					'container'=>"blank"
					,'content'=>"demo"
					,'email-to'=>["recipient@email.com"]
					// ,'email-to-cc'=>["recipient@email.com"]
					// ,'email-to-bcc'=>["recipient@email.com"]
					,'email-subject'=>"Subject"
					,'email-from'=>"sender@email.com"
					,'email-from-name'=>"Mr Email Sender"
				]
			],
			// [
            //     'type'=>'save-to-database'
            //     ,'database-credentials'=>[
            //         'host'=>'127.0.0.1'
            //         ,'database'=>'db_name'
            //         ,'table'=>'tb_name'
            //         ,'username'=>'root'
            //         ,'password'=>'root'
            //         ,'public-key'=>'public-8192-1618221780'
            //         ,'exclude-list-ids'=>['id-to-ignore']
            //     ]
            // ]
		]
	]);

	/**
	 * ADD FORM ELEMENTS
	 * label: (string)
	 * hide-label: (boolean)
	 * description: (string)
	 * element-type: (content,field,hiddenfield,multifield,select,checkbox,radio,button,textarea,recaptcha,submit)
	 * field-type: (default: text)(number,password,etc - used only for element-type of field)
	 * placeholder: (string)
	 * element-class: (string)
	 * container-class: (string)
	 * validation: (associative array: validator => validation fail message)
	 * 		validation types: 
	 * 			not-empty (field,checkbox,radio,textarea,multifield must have a value (checkbox at least 1))
	 * 			not-first-select-value (select value can not be first value)
	 * 			alpha-only (letters, dashes and spaces only)
	 * 			numeric-only (numbers, dashes and spaces only)
	 * 			alpha-numeric-only (letters, numbers, dashes and spaces only)
	 * 			valid-email (well formed email addresses only)
	 * 			valid-url (well formed URLs only)
	 * 			min-chars:4 (minimum of 4 characters - number can change eg. min-chars:6)
	 * 			max-chars:10 (maximum of 10 characters - number can change eg. max-chars:20)
	 * 			match-input:input-id (input matches the supplied id input Eg. input with id of input-id)
	 * 			valid-recaptcha (validates recaptcha)
	 * 			restrict-file-size:2mb (File uploads need to be under 2mb)
	 * html-content: array( array('tag','content','optionalElementClass'))
	 * element-content: (array or associative array)
	 * initial-value: (string) For select only
	 * g-recaptcha-data-site-key & g-recaptcha-secret: (string)
	 * response-displays: (associative array. response=>[targetId])
	 * 
	 * utm elements are added by default even if no utm data exists in url
	 */
	
	//content
	$form->addFormElement([
		'element-type'=>'content'
		,'html-content'=>[
			['h3',"Content"]
			,['p',"Hi {{name|User}}, Id quis in cillum ipsum ad laboris enim velit eiusmod aute. <a href='https://google.com' target='_blank'>hello world</a>."]
			,['p',"Elit commodo dolor dolor mollit incididunt do officia occaecat ea magna elit officia sit consectetur."]
		]
	]);

	//field
	$form->addFormElement([
		'label'=>"Field (with custom css)"
		,'element-type'=>'field'
		,'description-text'=>"Validators: not-empty, alpha-numeric-only, min-chars:4, max-chars:10"
		,'placeholder'=>"Enter your text here"
		,'element-class'=>"blue-text max-length-10"
		,'validation'=>[
			'not-empty'=>"Field can't be empty"
			,'alpha-numeric-only'=>"Letters and numbers only"
			,'min-chars:4'=>"Minimum of 4 characters"
			,'max-chars:10'=>"No more than 10 characters"
		]
	]);

	//field auto complete (see /js/demo.js)
	$form->addFormElement([
		'label'=>"Field autocomplete"
		,'id'=>'autocomplete'
		,'element-type'=>'field'
		,'placeholder'=>"Start typing for autocomplete"
	]);

	//multifield
	$form->addFormElement([
		'label'=>"Multi field"
		,'description-text'=>"Validators: not-empty, alpha-numeric-only"
		,'element-type'=>'multifield'
		,'placeholder'=>"Enter your text here"
		,'button-value'=>"Add field"
		,'validation'=>[
			'not-empty'=>"Field can't be empty (complete at least one field)"
			,'alpha-numeric-only'=>"Letters and numbers only"
		]
	]);

	//field numbers only with limits
	$form->addFormElement([
		'label'=>"Field with URL validation"
		,'description-text'=>"Validators: not-empty, valid-url"
		,'element-type'=>'field'
		,'placeholder'=>"Enter your text here"
		,'validation'=>[
			'not-empty'=>"Field can't be empty"
			,'valid-url'=>"Valid URLs only"
		]
	]);

	//field type="number"
	$form->addFormElement([
		'label'=>"Field with field type = number"
		,'description-text'=>"Validators: not-empty, numeric-only"
		,'element-type'=>'field'
		,'field-type'=>'number'
		,'validation'=>[
			'not-empty'=>"Field can't be empty"
			,'numeric-only'=>"Numbers only"
		]
	]);

	//field email validation
	$form->addFormElement([
		'label'=>"Field with email address validation"
		,'id'=>"email-address"
		,'description-text'=>"Id: email-address<br/>Validators: not-empty, valid-email"
		,'element-type'=>'field'
		,'placeholder'=>"Enter your email address"
		,'validation'=>[
			'not-empty'=>"Field can't be empty"
			,'valid-email'=>"Email address is not valid"
		]
	]);

	//field email validation
	$form->addFormElement([
		'label'=>"Retype Email Address"
		,'description-text'=>"Validators: not-empty, match-input:email-address"
		,'element-type'=>'field'
		,'placeholder'=>"Enter your email address"
		,'validation'=>[
			'not-empty'=>"Field can't be empty"
			,'match-input:email-address'=>"Email addresses must match"
		]
	]);


	//select
	$form->addFormElement([
		'label'=>"Select"
		,'description-text'=>"Validators: not-first-select-value"
		,'element-type'=>'select'
		,'initial-value'=>"Please select"
		,'element-content'=>[
			"Hello World"=>'hello-world'
			,"Goodbye World"=>'goodbye-world'
		]
		,'validation'=>[
			'not-first-select-value'=>"Select an option from the dropdown"
		]
	]);

	//select (data from php array)
	$form->addFormElement([
		'label'=>"Select using PHP array"
		,'element-type'=>'select'
		,'initial-value'=>"Please select"
		,'element-content'=>$countries
	]);

	//checkbox
	$form->addFormElement([
		'label'=>"Checkbox"
		,'description-text'=>"Validators: not-empty"
		,'element-type'=>'checkbox'
		,'element-content'=>[
			"Check one"=>'check-one'
			,"Check two"=>'check-two'
			,"Check three"=>'check-three'
		]
		,'validation'=>[
			'not-empty'=>'Select at least one checkbox'
		]
		,'response-displays'=>[
			'check-two'=>['check-two-selected']
		]
	]);

	//conditional content
	$form->addFormElement([
		'id'=>'check-two-selected'
		,'element-type'=>'content'
		,'html-content'=>[
			['p',"To check out the conditional elements demo <a href='demo-conditional.php' target='_blank'>Click here</a>."]
		]
	]);

	//radio
	$form->addFormElement([
		'label'=>"Radio"
		,'description-text'=>"Validators: not-empty"
		,'element-type'=>'radio'
		,'element-content'=>[
			"Select me"=>'select-me'
			,"or me"=>'or-me'
			,"or even me"=>'or-even-me'
		]
		,'validation'=>[
			'not-empty'=>"Select at least one radio button"
		]
	]);

	//button
	$form->addFormElement([
		'element-type'=>'button'
		,'button-value'=>"Button"
	]);

	//textarea
	$form->addFormElement([
		'label'=>"Textarea"
		,'description-text'=>"Validators: not-empty, alpha-numeric-only"
		,'element-type'=>'textarea'
		,'validation'=>[
			'not-empty'=>"Textarea can not be empty"
			,'alpha-numeric-only'=>"Letters and numbers only"
		]
	]);

	//file upload
	$form->addFormElement([
		'label'=>"File Upload"
		,'description-text'=>"Validators: not-empty, only-specific-file-types, restrict-file-size:2mb"
		,'element-type'=>'fileupload'
		,'file-upload-directory'=>'files/demo'
		,'accepted-file-types'=>"jpg jpeg png gif pdf zip"
		,'validation'=>[
			'not-empty'=>"File Upload can not be empty"
			,'only-accepted-file-types'=>"File type not accepted"
			,'restrict-file-size:2mb'=>"File uploads need to be under 2mb"
		]
	]);

	//Google recaptcha (requires Google V2 recaptcha account)
	// $form->addFormElement([
	// 	'element-type'=>'recaptcha'
	// 	,'g-recaptcha-data-site-key'=>'**********'
	// 	,'g-recaptcha-secret'=>'**********'
	// 	,'validation'=>[
	// 		'valid-recaptcha'=>"Please check recaptcha checkbox"
	// 	]
	// ]);

	//hidden field
	$form->addFormElement([
		'element-type'=>'hiddenfield'
		,'id'=>'hidden-field'
	]);

	//custom
	$form->addFormElement([
		'element-type'=>'custom'
		,'code'=>"<img style='width:100px;margin:20px 0 35px;' src='https://media.newyorker.com/photos/59095bb86552fa0be682d9d0/master/w_2240,c_limit/Monkey-Selfie.jpg'/>"
	]);

	//submit
	$form->addFormElement([
		'element-type'=>"submit"
		,'button-value'=>"Submit"
	]);

	//content (with defined class)
	$form->addFormElement([
		'element-type'=>'content'
		,'html-content'=>[
			['p',"Please note that all fields marked with an asterisk (*) are mandatory.",'footnote']
		]
	]);

	/**
	 * PROCESS FORM & OUTPUT VALIDATED HTML/CSS/JS
	 */

	$form->build();
