/**
 * On document ready
 */

 $(document).ready(function() {

	//prevent enter to submit (allow for textarea)
	$(window).keydown(function(event){
		if(event.keyCode == 13) {
			var target = event.target;
			if(target.type != 'textarea') {
				event.preventDefault();
				return false;
			}
		}
	});

	//set conditional elements functionality
	//radio
	$("input[type='radio']").click(function(){
		var conditionalChildren = ccIsValid($(this).attr("conditional-children"));
		toggleConditionalElements(this,conditionalChildren,$(this).attr("parent-id"));
	});
	//select
	$("select").change(function(){
		if($(this).attr('has-conditional-children')) {
			var conditionalChildren = ccIsValid($(this).find(":selected").attr("conditional-children"));
			toggleConditionalElements(this,conditionalChildren,$(this).attr("id"));
		}
	});
	//checkboxes
	$("input[type='checkbox']").click(function(){
		var conditionalChildren = ccIsValid($(this).attr("conditional-children"));
		toggleConditionalCheckboxElements(this,conditionalChildren,$(this).attr("parent-id"));
	});

	//set max input limits based on class names
	$('.max-length-2').attr('maxlength','2');
	$('.max-length-3').attr('maxlength','3');
	$('.max-length-4').attr('maxlength','4');
	$('.max-length-10').attr('maxlength','10');
	$('.max-length-20').attr('maxlength','20');
	$('.max-length-50').attr('maxlength','50');
	$('.max-length-100').attr('maxlength','100');
	$('.max-length-150').attr('maxlength','150');
	$('.max-length-200').attr('maxlength','200');
	$('.max-length-300').attr('maxlength','300');
	$('.max-length-400').attr('maxlength','400');
	$('.max-length-500').attr('maxlength','500');
	
});

/**
 * conditional element functions
 */

 function toggleConditionalElements(target,displayIds,removeClass){	
	//split displayIds into an array
	displayIdsArray = (displayIds.length > 0) ? displayIds.split(" ") : [];

	//cycle through all instances of existing removeClass(es) and remove all that are not in the displayIds array
	hideElements(displayIdsArray,removeClass);

	//cycle through all instances of displayIds and show
	showElements(displayIdsArray);
}

function hideElements(displayIds,removeClass) {
	//cycle through all instances of existing removeClass(es) and remove all that are not in the displayIds array
	$('.'+removeClass).each(function(){
		var hideChild = true;
		for(j=0;j<displayIds.length;j++) {
			if($(this).attr('id') == "element-container-"+displayIds[j]) {
				hideChild = false;
			};
		}
		if(hideChild){hideElement(this);}
	});
}

function hideElement(target) {
	//hide element
	$(target).slideUp();
	//remove inputs from radio buttons
	$(target).find("input[type='radio']").prop('checked', false);
	//remove inputs from select dropdown
	$(target).find('select').val($(target).find('select option:first').val());
	//remove inputs from checkboxes
	$(target).find("input[type='checkbox']").prop('checked', false);
	//remove validation alert
	$(target).find(".validation-error").html('');
	$(target).find(".validation-error").removeClass('validation-error');
}

function showElements(displayIds) {
	//cycle through all instances of displayIds and show
	for(i=0;i<displayIds.length;i++) {
		if(!$('#element-container-'+displayIds[i]).is(":visible")) {
			showElement(displayIds[i])
		}
	}
}

function showElement(displayId) {
	if(!$('#element-container-'+displayId).is(":visible")) {
		$('#element-container-'+displayId).slideDown();
		$('#element-container-'+displayId).trigger("conditional-element-displayed",['#element-container-'+displayId]);
	}
}

function toggleConditionalCheckboxElements(target,displayIds,removeClass){
	//split displayIds into an array
	displayIdsArray = (displayIds.length > 0) ? displayIds.split(" ") : [];

	if($(target).is(":checked")) {
		showElements(displayIdsArray);
	} else {
		
		for(i=0;i<displayIdsArray.length;i++){
			hideElement($('#element-container-'+displayIdsArray[i]));
			//hide element's children
			hideElements([],displayIdsArray[i]);
		}
	}
}

function ccIsValid(ccString) {
	return (ccString != null && ccString != 'undefined' && ccString.length > 0) ? ccString : '';
}

function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
        }
    }
    return false;
};