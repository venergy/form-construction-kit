# Form Construction Kit 

## Version: 2.21
Build forms with validation and email/database integration from PHP configuration files. 

Use templates to style forms and emails.

Check out [demo.php](https://kineticvenergy.com/form-construction-kit/demo.php) or [demo-conditional.php](https://kineticvenergy.com/form-construction-kit/demo-conditional.php) for detailed working examples.

## Requirements
- PHP 8.*
- Composer

## Installation 
- Download the contents of this repository to your root folder.
- Run 'Composer install' to retrieve the project's dependencies.
- Create a new PHP form configuration file (eg. demo.php) at the root level. 

## Load Form Construction Kit
Using the new form configuration file (eg. demo.php).
```php
use Form\FormConstructionKit;
require_once "FormConstructionKit/FormConstructionKit.php";
```

## Instantiate new form object
The FormConstructionKit class accepts an associative array of form options. The form-template option is the only mandatory option.
### Basic instantiation example
```php
$form = new FormConstructionKit([
	'form-template'=>"blank"
]);
```
### Complex instantiation example with all options
```php
$form = new FormConstructionKit([
	'form-template'=>"blank"
	,'css'=>['bootstrap.min','global']
	,'js'=>['bootstrap.bundle.min','global','countries','demo']
	,'template-variables'=>[
		'meta-title'=>"Form Construction Kit | Form Construction Kit"
		,'google-analytics-tracking-id'=>"**-********-*"
		,'google-tag-manager-tracking-id'=>"***-*******"
		,'header-text'=>"Form Construction Kit Demo"
		,'submit-fail-text'=>"There are {% INVALID_ELEMENT_COUNT %} errors with your submission. Please see below."
		,'submit-success-html'=>[
			['h2',"Success"]
			,['p',"Thank you for your submission. We have sent you a confirmation email."]
		]
	]
	,'submit-success-actions'=>[
		[
			'type'=>"send-email"
			,'email-template'=>[
				'container'=>"blank"
				,'content'=>"demo"
			]
		],
		[
			'type'=>'save-to-database'
			,'database-credentials'=>[
				'host'=>'127.0.0.1'
				,'database'=>'db_name'
				,'table'=>'tb_name'
				,'username'=>'root'
				,'password'=>'root'
				,'public-key'=>'public-8192-1618221780'
				,'exclude-list-ids'=>['id-to-ignore']
			]
		]
	]
]);
```

## Add form elements
Add a series of form elements to your form using addFormElement. Define an element by it's element-type. A list of element types are outlined at the bottom of this ReadMe.
### Basic field example
```php
$form->addFormElement([
	'element-type'=>'field'
]);
```

### Complex field example with all options
```php
$form->addFormElement([
	'label'=>"Field Label"
	,'id'=>'field-label'
	,'element-type'=>'field'
	,'field-type'=>'text' //text(default),number,password,etc
	,'description-text'=>"Officia adipisicing <a href='https://google.com' target='_blank'>elit</a> elit."
	,'placeholder'=>"Enter your text here"
	,'element-class'=>"custom-class"
	,'button-value'=>"Button" //button & multifield elements only
	,'validation'=>[
		'not-empty'=>"Field can't be empty"
		,'alpha-only'=>"Letters only please"
		,'min-chars:4'=>"Minimum of 4 characters"
	]
]);
```

## Element content
Element content is used to populate radio, select and checkbox elements. The Array type can either be an Array or Associative Array. Associative Array defines "display"=>"value".
```php
$form->addFormElement([
	'element-type'=>'select'
	,'initial-value'=>"Please select"
	,'element-content'=>[
		"Select 1"=>'select-1'
		,"Select 2"=>'select-2'
	]
	,'validation'=>[
		'not-first-select-value'=>"Select an option from the dropdown"
	]
]);
```

Using a pre defined PHP array.
```php
$continents = ['Asia','Africa','North America','South America','Antartica','Europe','Australia'];

$form->addFormElement([
	'element-type'=>'select'
	,'initial-value'=>"Please select"
	,'element-content'=>$continents
	,'validation'=>[
		'not-first-select-value'=>"Select an option from the dropdown"
	]
]);
```

## Conditional elements
Elements such as radio, select and checkbox can display and hide other element based on the user's selected answer(s) using 'response-displays'. Check out [demo-conditional.php](https://kineticvenergy.com/form-construction-kit/demo-conditional.php) for a more detailed working example.
```php
$form->addFormElement([
	'label'=>"Radio Label"
	,'element-type'=>'radio'
	,'element-content'=>[
		"Yes"=>'yes'
		,"No"=>'no'
	]
	,'validation'=>[
		'not-empty'=>"Select at least one radio button"
	]
	,'response-displays'=>[
		'yes'=>['target-id-1']
		,'no'=>['target-id-2','target-id-3']
	]
]);

$form->addFormElement([
	'label'=>"Field 1"
	,'id'=>'target-id-1'
	,'element-type'=>'field'
]);

$form->addFormElement([
	'label'=>"Field 2"
	,'id'=>'target-id-2'
	,'element-type'=>'field'
]);

$form->addFormElement([
	'label'=>"Field 3"
	,'id'=>'target-id-3'
	,'element-type'=>'field'
]);
```

## Variable text
URL parameters can be inserted into content text via the use of curly braces in the form of {{variableName|defaultFallbackValue}}. 
Supported content types include 'html-content', 'placeholder', 'validation messages' & 'submit-success-html'.
All URL parameters are sanitised before output.
```php
$form->addFormElement([
	'element-type'=>'content'
	,'html-content'=>[
		['p',"Hi {{name|User}}, Id quis in cillum ipsum ad laboris enim velit eiusmod aute."]
	]
]);
```

## File upload
Implements a file upload element.
<sup>Confirm your version of PHP allows file uploads via your php.ini file (Set file_uploads = On & upload_max_filesize = XXM)</sup>
```php
$form->addFormElement([
	'element-type'=>'fileupload'
	,'file-upload-directory'=>'files/demo'
	,'accepted-file-types'=>"jpg jpeg gif pdf zip"
	,'validation'=>[
		'not-empty'=>"File Upload can not be empty"
		,'only-accepted-file-types'=>"File type not accepted"
		,'restrict-file-size:2mb'=>"File uploads need to be under 2mb"
	]
]);
```
<sup>Note: restrict-file-size:2mb. '2mb' represents a variable value. Eg. 2kb, 2mb, etc.</sup>

## Custom content
Allows insertion of HTML.
```php
$form->addFormElement([
	'element-type'=>'custom'
	,'code'=>"<img style='width:100px;margin:20px 0 35px;' src='https://media.newyorker.com/photos/59095bb86552fa0be682d9d0/master/w_2240,c_limit/Monkey-Selfie.jpg'/>"
]);
```

## Recaptcha
Implements the Recaptcha V2 challenge. 
```php
$form->addFormElement([
	'element-type'=>'recaptcha'
	,'g-recaptcha-data-site-key'=>'**********'
	,'g-recaptcha-secret'=>'**********'
	,'validation'=>[
		'valid-recaptcha'=>"Please check recaptcha checkbox"
	]
]);
```

## Submit
Include a submit element (button) so the form can be submitted.
```php
$form->addFormElement([
	'element-type'=>"submit"
	,'button-value'=>"Submit"
]);
```
	
## Process form and output validated html/css/js
It is mandatory to include this line at the bototm of your code to output your form.
```php
$form->build();
```

## Form templates
Defining a form template in the initial FormConstructionKit instantiation array is required. Form templates are stored in the 'form-template' directory. Form templates are a combination of HTML and PHP. The example (form-template/blank.php) displays some basic usage retrieving data which is declared in the initial FormConstructionKit instantiation array.

## CSS & JS inclusions
CSS and JS files are stored in their respecive root directories. They're included in the form by declaration in the initial FormConstructionKit instantiation array.

## Email sending and templates
Emails can be send on successful submission. Submission emails are defined in the 'submit-success-actions' in the initial FormConstructionKit instantiation array. Email templates are stored in the 'email-template' directory. They are broken up into 'container' and 'content'. The 'container' is the HTML component of the email and the 'content' is injected into the 'container' via a {% TITLE %} and {% BODY %} tag.

## DKIM (DomainKeys Identified Mail)
Emails can be authenticated using DKIM. Public/private keys can be created using FormConstructionKit/GenerateDkim.php. DKIM variables should be declaired in the /email-template/content/&lt;filename&gt;.php file.
```php
$dkimOn = true;
$dkimDomain = 'example.com';
$dkimPrivateKey = "<private key location>";
$dkimSelector = '<selector name>';
```
Your private key should not be publicly accessible. If storing the private key in /dkim then use the following .htaccess rewrite rule to restrict access to this directory: 
RewriteRule dkim/* — [F,L,NC]

## Save submission data to a database
Submission data can be stored in a database on successful submission. Use the table schema found in docs/db/table-structure.sql and add the 'submission-success-action' of 'save-to-database' (found in the example 'Instantiate new form object'). This action can save the submission data as encrypted serialized data and requires a public/private keys which are stored in the keys directory. Keys can be generated using FormConstructionKit/GenerateKeys.php. **DO NOT** use the public/private keys provided in this repository - they are for demonstration purposes only. Setting the public-key to an empty string will save the data as unencrypted serialized data. Eg. 'public-key'=>''.

## Validate Email Address
Within your email-template/content/\<filename\>.php use the following PHP code:
```php
echo $this->getEmailVerificationLink();
```

This will return a unique validation URL of:
```
<filename>-validate.php?sdt=submission-date-time&suid=submission-uid

eg: demo-validate.php?sdt=1621311372&suid=0932fa79355
```

When a user clicks this link in an email - \<filename\>-validate.php searches for a match in the database. If a match is found then the row's 'valid' column is changed from 0 to 1.

## Element types
This is a list of the accepted element types.

- content
- field
- hiddenfield
- multifield
- select
- checkbox
- radio
- button
- textarea
- hiddenfield
- fileupload
- custom
- recaptcha
- submit

## Validation types
This is a list of the accepted validation types. Multiple validations can be assigned to a single form element. The FormConstructionKit validator will validate each and only display the last failed validation message.

- not-empty (field,checkbox,radio,textarea & multifield must have a value (checkbox at least 1))
- not-first-select-value (select value can not be first value)
- alpha-only (Upper and lower case letters, dashes and spaces only)
- numeric-only (numbers, dashes and spaces only)
- alpha-numeric-only (Upper and lower case letters, numbers, dashes and spaces only)
- valid-email (well formed email addresses only)
- valid-url (well formed URLs only)
- min-chars:4 (minimum of 4 characters - number can change eg. min-chars:6)
- max-chars:10 (maximum of 10 characters - number can change eg. max-chars:20)
- match-input:input-id (input matches the supplied id input Eg. input with id of input-id)
- valid-recaptcha (validates recaptcha)
- only-accepted-file-types (only accept file with an extension defined in 'accepted-file-types')
- restrict-file-size:2mb. ('2mb' represents a variable value. Eg. 2kb, 2mb, etc.)
